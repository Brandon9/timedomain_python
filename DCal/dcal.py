#!/usr/local/bin/python
#import math
import numpy as np
import Tkinter, tkFileDialog
import os
import itertools
import matplotlib.pyplot as plt
import pdb
import sys
import argparse

nArgs = len(sys.argv)
print 'Num args:', nArgs
print 'Arg list:', str(sys.argv)

# Optional command line args for nodeIDs and truth locations
# If these are not entered then they are assumed to be the first two lines of the def logfile
# in this format (without comment chars):
#NodeIDs, 107, 111, 114
#Rtruth, 8.0992, 9.2226, 11.1872

if nArgs != 1 : 

    parser = argparse.ArgumentParser(description='Distributed calibration.')
    parser.add_argument('-n', dest='NodeIDs', type=int, nargs='+', help='a list of nodeIDs')
    parser.add_argument('-d', dest='Rtruth', type=float, nargs='+', help='a list of link distances')
    args = parser.parse_args()
    argdict = vars(args)
    NodeIDs = argdict['NodeIDs']
    Rtruth = argdict['Rtruth']

    print 'NodeIDs:',
    print '%d '*len(NodeIDs) % tuple(NodeIDs)
    print 'Rtruth:',
    print '%d '*len(Rtruth) % tuple(Rtruth)

#pdb.set_trace()

def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False
        
def getInput() :
    
    hDialog = Tkinter.Tk()
    hDialog.withdraw() # don't want a full GUI
    hDialog.update()
    ASK_FOR_FN = 1
    if ASK_FOR_FN :
        fname_in = tkFileDialog.askopenfilename(parent=hDialog,title="Choose an input file",filetypes=[("Log files","*.csv")])
    else :
        fname_in = 'Outdoor_Bias_FCC_Dataset_-106bins_Set2_short.csv';
    print(fname_in)
    #drive,path = os.path.splitdrive(fname_in)
    #path,filename = os.path.split(path)
    path,filename = os.path.split(fname_in)
    print('Path:%s, File:%s' % (path, filename))
    return [path, filename, hDialog]
    
# def readDataBak(fullfile_in) :
#     rlist_map = [['nan',0,1],[0,'nan',2],[1,2,'nan']]
#     #nodeID_list = [100, 101, 102]
#     rlist = np.empty((1000,3))
#     rlist[:] = np.NAN
#
#     iline = 0
#     ir = 0
#     imeas = 0
#     D = dict()
#     with open(fullfile_in,'r') as f:
#         for line in f:
#             iline += 1
#             #print "%d: %s" % (k,line),
#             line = line.rstrip()  # remove linefeed if it exists
#             if line == '':
#                 continue
#
#             linelist = line.split(",")
#             print "%3d: %s" % (iline,linelist)
#
#             if 'Timestamp' == linelist[0]:
#                 continue
#
#             if 'Nodelist' == linelist[0]:
#                 nodeIDs = map(int,linelist[1:])
#                 nNodes = len(nodeIDs)
#                 nLinks = nNodes*(nNodes-1)/2
#                 #rArray = np.empty([1,nLinks])
#                 #Rmeas = [np.nan for i in range(0,nLinks)]
#                 Rm_array = np.empty((1,nLinks))
#                 Rm_array[:] = np.nan
#                 D.update({'NodeList':nodeIDs})
#
#             if 'Rtrue' == linelist[0] :
#                 Rtrue = map(float,linelist[1:])
#                 print 'Rtrue: [',
#                 #for i in range(0,len(Rtrue) :
#                 print Rtrue
#                 D.update({'Rtrue':Rtrue})
#
#             if " RcmEchoedRangeInfo" == linelist[1]:
#                 #print 'RcmEchoedRangeInfo'
#                 reqID = int(linelist[3])
#                 rspID = int(linelist[4])
#                 rmeas = float(linelist[5])/1000.0
#                 ree = float(linelist[6])/1000.0
#                 led_flags = int(linelist[7])
#                 print "reqID:%3d, rspID:%3d, rmeas:%7.3f, ree:%.3f, led_flags:%d" % (reqID,rspID,rmeas,ree,led_flags)
#                 if led_flags != 8 or ree > 60 :
#                     print "led_flag =%d, ree=%.3f skipping ..." % (led_flag, ree)
#                     continue
#
#                 if 'RcmEchoedRangeInfo' in D :
#                     D['RcmEchoedRangeInfo'].append([reqID,rspID,rmeas,ree,led_flags])
#                 else :
#                     D['RcmEchoedRangeInfo'] = [[reqID,rspID,rmeas,ree,led_flags]]
#
#                 reqID_index = nodeIDs.index(reqID)
#                 rspID_index = nodeIDs.index(rspID)
#                 print "reqID_index:%d, rspID_index:%d" % (reqID_index, rspID_index)
#                 rlist_index = rlist_map[reqID_index][rspID_index]
#                 ilink = rlist_map[reqID_index][rspID_index]
#                 print "rlist_index %d" % rlist_index
#                 rlist[ir][rlist_index] = rmeas
#                 newrow = np.empty((1,nLinks))
#                 newrow[:] = np.nan
#                 newrow[0][rlist_index] = rmeas
#                 Rm_array = np.append(Rm_array,newrow,axis=0)
#                 #Rm_array[imeas][ilink] = rmeas
#
#                 imeas += 1
#                 ir += 1
#
#     # Remove all the nans and make a list of lists
#     Rmeas = []
#     Rmean = np.empty(nLinks)
#     Rstd = np.empty(nLinks)
#     Rlen = np.empty(nLinks)
#     for ilink in range(0,nLinks) :
#         Rmeas.append(Rm_array[~np.isnan(Rm_array[:,ilink]),ilink])
#         Rmean[ilink] = np.mean(Rmeas[ilink])
#         Rstd[ilink] = np.std(Rmeas[ilink])
#         Rlen[ilink] = len(Rmeas[ilink])
#
#     # Trim to shortest number of link measurements
#     for ilink in range(0,nLinks) :
#         Rmeas[ilink] = Rmeas[ilink][0:min(Rlen)]
#
#     pdb.set_trace()
#     return [nodeIDs,Rtrue,Rmeas,Rmean,Rstd]

def readData(fullfile_in) :
    D = dict()
    iline = 0
    with open(fullfile_in,'r') as f:
        for line in f:
            iline += 1
            #print "%d: %s" % (k,line),
            line = line.rstrip()  # remove linefeed if it exists
            if line == '':
                continue
            
            linelist = line.split(",")
            #print "%3d: %s" % (iline,linelist)

            if 'Timestamp' == linelist[0]:
                continue

            if 'NodeIDs' == linelist[0]:
                NodeIDs = map(int,linelist[1:])
                print 'NodeIDs: ',
                print NodeIDs
                D.update({'NodeIDs':NodeIDs})

            if 'Rtruth' == linelist[0] :
                Rtruth = map(float,linelist[1:])
                print 'Rtruth: ',
                print Rtruth  
                D.update({'Rtruth':Rtruth})

            if " RcmEchoedRangeInfo" == linelist[1]:
                #print 'RcmEchoedRangeInfo'
                reqID = int(linelist[3])
                rspID = int(linelist[4])
                rmeas = float(linelist[5])/1000.0
                ree = float(linelist[6])/1000.0
                ledflags = int(linelist[7])
                #print "reqID:%3d, rspID:%3d, rmeas:%7.3f, ree:%.3f, led_flags:%d" % (reqID,rspID,rmeas,ree,led_flags)
                #if led_flags != 8 or ree > 60 :
                #    print "led_flag =%d, ree=%.3f skipping ..." % (led_flag, ree)
                #    continue
                
                if 'RcmEchoedRangeInfo' in D :
                    D['RcmEchoedRangeInfo'].append([reqID, rspID, rmeas, ree, ledflags])
                else :
                    D['RcmEchoedRangeInfo'] = [[reqID, rspID, rmeas, ree, ledflags]]
                            
    return D

def getLinkInfo(node_pair,rangeInfoAll,nodeIDs) :
    n0 = node_pair[0]
    n1 = node_pair[1]
    node0_index = nodeIDs.index(n0)
    node1_index = nodeIDs.index(n1)
    #linkArray = []
    first = True
    #np.zeros((1,5),dtype=('i, i, f,f,i'))
    
    for imeas in range(0,len(rangeInfoAll)) :
        rinfo_tmp = rangeInfoAll[imeas]
#        print link_tmp

#        linkList = []
        if (rinfo_tmp[0] == n0 and rinfo_tmp[1] == n1) or (rinfo_tmp[0] == n1 and rinfo_tmp[1] == n0) :
            #print 'Found %d <-> %d' % (nodeID0,nodeID1)
            #linkArray.append(tuple(link_tmp))
            if first :
                first = False
                rinfo_array = [rinfo_tmp]
            else :
                rinfo_array.append(rinfo_tmp)
            
#            pdb.set_trace()

    rinfo_array = np.array(rinfo_array)
    return rinfo_array

def filtRanges(rinfo_raw) :
    ranges = rinfo_raw[:,2]
    rees = rinfo_raw[:,3]
    ledflags = rinfo_raw[:,4]
    rfilt_tmp = np.ma.masked_where(rees>.060,ranges)
    rfilt = np.ma.masked_where(np.not_equal(ledflags,8),rfilt_tmp)
    return rfilt.tolist()

  
def plotRanges(nodeIDs,Rmeas,Rmean,) :
    plt.figure(1)
    ax = plt.subplot(2,1,1)
    plt.plot(Rmeas[0],'b-')
    plt.hold(True)
    plt.plot([0, len(Rmeas[0])],[Rmean[0], Rmean[0]],color='k')
    plt.plot(Rmeas[1],'r-')
    plt.plot([0, len(Rmeas[1])],[Rmean[1], Rmean[1]],color='k')
    plt.plot(Rmeas[2],'g-')
    plt.plot([0, len(Rmeas[2])],[Rmean[2], Rmean[2]],color='k')
    ax.set_xlim(0,len(Rmeas[0]))
    plt.grid(True)
    plt.title('Ranges between each Node')

def plotBiases(nodeIDs,Emat,Emean) :
    ax = plt.subplot(2,1,2)
    plt.plot(Emat[0],'b-',label='E0')
    plt.hold(True)
    (nrows,ncols) = Emat.shape
    plt.plot([0, ncols],[Emean[0], Emean[0]],color='k')
    plt.plot(Emat[1],'r-',label='E1')
    plt.plot([0, ncols],[Emean[1], Emean[1]],color='k')
    plt.plot(Emat[2],'g-',label='E2')
    plt.plot([0, ncols],[Emean[2], Emean[2]],color='k')
    ax.set_xlim(0,ncols)
    plt.grid(True)
    plt.legend()
    plt.title('Error Biases for each Node')


# Here's the main
[path, filename, hDialog] = getInput()

D = readData(os.path.join(path,filename))
if 'NodeIDs' in D :
    NodeIDs = D['NodeIDs']
nNodes = len(NodeIDs)
nLinks = nNodes*(nNodes-1)/2

Rmeas = []
Rmeans = []
lengths = []
for node_pair in list(itertools.combinations(NodeIDs,2)) :
    rinfo_raw = getLinkInfo(node_pair,D['RcmEchoedRangeInfo'],NodeIDs)
    rfilt = filtRanges(rinfo_raw)
    Rmeas.append(rfilt)
    lengths.append(len(rfilt))
    Rmeans.append(np.mean(rfilt))
    
nTrim = min(lengths)
for k in range(0,nLinks) :
    Rmeas[k] = Rmeas[k][0:nTrim]

# Plot ranges with means
plt.ion()  # This turns on interactive plots
plotRanges(NodeIDs,Rmeas,Rmeans)

# Create X - the design matrix, and Xinv
X = np.array([[1, 1, 0],[1, 0, 1],[0, 1, 1]])
#Xinv = ((X'*X)^-1)*X';
Xinv = np.linalg.inv(X)

#Rt_mat = np.repmat(R_true
if 'Rtruth' in D :
    Rtruth = np.array(D['Rtruth'])
else :
    Rtruth = np.array(Rtruth)
    
Rtruth = Rtruth[:,None]  # Transposes a one-dimensional

Rmeas = np.array(Rmeas)
(nrows,ncols) = Rmeas.shape
Rtruth_ext = np.tile(Rtruth,(1,ncols))

Rdiff = (Rtruth_ext - Rmeas)
#Emat = Xinv*(Rt-Rm);
Earray = np.dot(Xinv,Rdiff)

Emean = np.mean(Earray,axis=1)
Estd = np.std(Earray,axis=1)

plotBiases(NodeIDs,Earray,Emean)
#plt.draw()

 
# Convert E to bins
psPerBin = 1.907
psPerBin = 1000./512.

c_mps = 299792458/1.0003 # speed of RF in air
psPerSec = 1e12
binsPerMeter = psPerSec/c_mps/psPerBin
Ebins = Emean*binsPerMeter
Ebins_adj = round(2*Ebins)

# Print E estimates
Emean_mm = Emean*1000
Estd_mm = Estd*1000
print '\nNodeI  NodeID       Emean (mm)      Estd (mm)         Ebins\n'
for i in range(0,len(Ebins)) :
    print '%d       %d     %12.5f    %12.5f   %10.0f' % (i,NodeIDs[i],Emean_mm[i],Estd_mm[i],Ebins[i])

print ''
print 'Mean            %12.5f    %12.5f   %10.0f' % (np.mean(Emean_mm),np.mean(Estd_mm),np.mean(Ebins))
print 'Max             %12.5f    %12.5f   %10.0f' % (np.max(Emean_mm),np.max(Estd_mm),np.max(Ebins))
print 'Min             %12.5f    %12.5f   %10.0f' % (np.min(Emean_mm),np.min(Estd_mm),np.min(Ebins))


raw_input("Press any key to exit: ")
#plt.show()
#pdb.set_trace()







            