#!/usr/local/bin/python
#import math
import numpy as np
#from numpy.linalg import inv
#from numpy import dot
import os
#import itertools
#import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import Tkinter, tkFileDialog
import time
#import pylab
import pdb
import sys, getopt, tty, termios
from processMsg import processMsg
from myKalman import EKF2D
#import pprint

axisLimits = [-2, 23, -6, 24]
#axisLimits = []

def main(argv) :
    
    inputfile = '/Volumes/Transcend/Data/GameData_Oct2015/Game_Run_Test_NodeID_102_JL_BB_005.csv'
#    inputfile = ''
    outputfile = ''
    if not inputfile :
        try:
           opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
        except getopt.GetoptError:
           print 'test.py -i <inputfile> -o <outputfile>'
           sys.exit(2)
        for opt, arg in opts:
           if opt == '-h':
              print 'test.py -i <inputfile> -o <outputfile>'
              sys.exit()
           elif opt in ("-i", "--ifile"):
              inputfile = arg
           elif opt in ("-o", "--ofile"):
              outputfile = arg
    print 'Input file is "', inputfile
    print 'Output file is "', outputfile
    
#    inputfile = 'navNet_ELL_short.csv'
#    inputfile = 'navNet_ELLEX_001.csv'
#    inputfile = []  # leave blank to query the user

    if not inputfile :  # if empty
        inputfile = queryUser()
    
    gui = gui2D(inputfile,axisLimits,trailLen=100)
    map = LMAP()
#    ekf = EKF2D(X=[],P=[],aee=0.1,zFixed=1.233)
    ekf = EKF2D(X=[15,0,-10,0],P=None,aee=0.1,zFixed=1.233)
    
    with open(inputfile,'r') as f:
        iline = 0

        for line in f:
            iline += 1
            print '\n%d: %s' % (iline,line[0:-1])
            
            msg = processMsg(line)

            if not msg : # msg is empty
                continue

#            print msg
            
            if 'RcmGetStatusInfoConfirm' == msg['msgType'] :
                rcmStatus = msg
                
            elif 'RcmP3XXConfig' == msg['msgType'] :
                rcmConfig = msg
                thisNodeID = rcmConfig['NodeId']
                radioType = 'P3XX'
                
            elif 'RcmConfig' == msg['msgType'] :
                rcmConfig = msg
                thisNodeID = rcmConfig['NodeID']
                radioType = 'P4XX'
                
            elif 'RnGetConfigConfirm' == msg['msgType'] :
                rnConfig = msg
                
            elif 'RnGetAlohaConfigConfirm' == msg['msgType'] :
                rnAlohaConfig = msg
                
            elif 'NnConfig' == msg['msgType'] :
                nnConfig = msg

            elif 'NnLocationMapEntry' == msg['msgType'] :
                # update lmap and plot
                map.add(msg['NodeID'],msg['NodeType'],msg['X'],msg['Y'],msg['Z'])
                gui.drawAnchor(msg['NodeID'],msg['X'],msg['Y'],msg['Z'])
                
            elif 'NnWaypointEntry' == msg['msgType'] :
                # update lmap and plot
                map.add(msg['ID'],msg['NodeType'],msg['X'],msg['Y'],msg['Z'])
                gui.drawWaypoint(msg['ID'],msg['X'],msg['Y'],msg['Z'])
                    
            elif 'RcmEchoedRangeInfo' == msg['msgType'] :
                rangeInfo = msg
                gui.drawRangeLine(rangeInfo['RequesterID'],rangeInfo['ResponderID'],map)
########################################        
#                pdb.set_trace()       
########################################
                timestamp = rangeInfo['Timestamp']
                rMeas = rangeInfo['PrecisionRangeMm']/1000.
                ree = rangeInfo['PrecisionRangeErrEstMm']/1000.
                rspLoc = map.getLoc(rangeInfo['ResponderID'])
                ekf.update(timestamp,rMeas,ree,rspLoc)
                plt.plot(ekf.X[0],ekf.X[2],'yx')
                plt.draw()
                
            elif 'RcmRangeInfo' == msg['msgType'] :
                rangeInfo = msg
                gui.drawRangeLine(thisNodeID,rangeInfo['ResponderID'],map)
                # ekf.update(rangeInfo['Timestamp'],rangeInfo['PrecisionRange'],rangeInfo['PrecisionRangeErrEst'],map.getLoc(rangeInfo['ResponderID']))
                # plt.plot(ekf.X[0],ekf.X[2],'bo')
                # plt.draw()
                
            elif 'NnLocationInfo' == msg['msgType'] :
                # update lmap and plot
#                currentLoc = np.array([msg['X'],msg['Y'],msg['Z']])
                map.update(msg['NodeID'],msg['X'],msg['Y'],msg['Z'])
                gui.drawMobile(msg['MessageID'],msg['NodeID'],msg['X'],msg['Y'],msg['Z'])
                gameInfo['elapsedTime'] = msg['Timestamp'] - gameInfo['startTime']
                if gameInfo :
                    gui.updateTime(gameInfo['elapsedTime'])
                
            elif 'NnEchoedLocationInfo' == msg['msgType'] :
                # update lmap and plot
#                currentLoc = np.array([msg['X'],msg['Y'],msg['Z']])
                map.update(msg['NodeID'],msg['X'],msg['Y'],msg['Z'])
                gui.drawMobile(msg['MessageID'],msg['NodeID'],msg['X'],msg['Y'],msg['Z'])
                # TODO: add traces in gui.drawMobile

            elif 'NnEchoLastLocationExInfo' == msg['msgType'] :
                # update lmap and plot
#                currentLoc = np.array([msg['X'],msg['Y'],msg['Z']])
                map.update(msg['NodeID'],msg['X'],msg['Y'],msg['Z'])

                if msg['LocationErrorEstimate'] > 1000 :
                    print 'LEE=%.3f, R=%.3f, rspID=%d' % (float(msg['LocationErrorEstimate'])/1000.,float(rangeInfo['PrecisionRangeMm'])/1000.,rangeInfo['ResponderID'])
                    gui.drawMobile(msg['MessageID'],msg['NodeID'],msg['X'],msg['Y'],msg['Z'],'r')

                    rspLoc = map.getLoc(rangeInfo['ResponderID'])
                    circle = plt.Circle((rspLoc[0],rspLoc[1]),radius=float(rangeInfo['PrecisionRangeMm'])/1000.,color='r',fill=False) 
                    fig = plt.gcf()
                    fig.gca().add_artist(circle)
                    plt.draw()
########################################        
                    pdb.set_trace()       
########################################
                else :
                    gui.drawMobile(msg['MessageID'],msg['NodeID'],msg['X'],msg['Y'],msg['Z'],'c')               
                    
                                                    
            elif 'GameConfig' == msg['msgType'] :
                gameInfo = msg
                gameInfo['mobileID'] = 112
                gameInfo['wpList'] = [msg['Waypoint1'],msg['Waypoint2'],msg['Waypoint3']]
                gameInfo['errors'] = [0, 0, 0]                
                gameInfo['markerCount'] = 0
                gameInfo['startTime'] = msg['Timestamp']
                gameInfo['elapsedTime'] = 0
                gameInfo['elapsedTime_prev'] = gameInfo['elapsedTime']
                gameInfo['timestamps'] = [msg['Timestamp'], 0, 0, 0]
                print 'GAME CONFIG: Walker: %s, Caller: %s Waypoints: [%d,%d,%d]' % (msg['WalkerInitials'],msg['CallerInitials'],msg['Waypoint1'],msg['Waypoint2'],msg['Waypoint3'])
                gui.markWaypoints(gameInfo['wpList'],map)
                
            elif 'LogfileMarker' == msg['msgType'] :
                #print 'LogfileMarker - Compute and store slant range error here'
                wpIndex = msg['markerNum']-1
                wpID = gameInfo['wpList'][wpIndex]
                mobID = gameInfo['mobileID']               
                wpLoc = np.array(map.getLoc(wpID))
                mobLoc = np.array(map.getLoc(mobID))
                plt.plot(mobLoc[0],mobLoc[1],'*',markersize=10,markerfacecolor='k',hold=True)
                slantErr = np.linalg.norm(wpLoc-mobLoc)
                gameInfo['errors'][wpIndex] = slantErr
                gameInfo['markerCount'] += 1
                gameInfo['elapsedTime'] = msg['Timestamp'] - gameInfo['startTime']
                gameInfo['splitTime'] = gameInfo['elapsedTime'] - gameInfo['elapsedTime_prev']
                gameInfo['elapsedTime_prev'] = gameInfo['elapsedTime']
                gameInfo['timestamps'][wpIndex+1] = msg['Timestamp']
                plt.text(mobLoc[0]+1.,mobLoc[1]+1.5,'Split Time: %5.3f s' % gameInfo['splitTime'],fontsize=14)           
                plt.text(mobLoc[0]+1.,mobLoc[1]+0.5,'Split Error: %5.3f m' % slantErr,fontsize=14)             
                if gameInfo['markerCount'] == 3 :
                    # game over
                    totalTime = msg['Timestamp'] - gameInfo['startTime']
                    totalError = sum(gameInfo['errors'])
                    meanError = np.mean(gameInfo['errors'])
                    plt.text(mobLoc[0]+1.,mobLoc[1]-1.0,'Total Time: %5.2f s' % totalTime,fontsize=14)
                    plt.text(mobLoc[0]+1.,mobLoc[1]-2.0,'Total Error: %5.3f m' % totalError,fontsize=14)
                    plt.text(mobLoc[0]+1.,mobLoc[1]-3.0,'Mean Error: %5.3f m' % meanError,fontsize=14)
                    print 'Total time = %5.3f' % totalTime         
                    print 'Total error (m) = %6.4f' % totalError
                    print 'Mean error (m) = %6.4f' % meanError
                    response = raw_input("Press 'c' to continue, any other key to exit >>> ")

                    if not response or response[0] != 'c' :
                        return
            
            else :
                print 'UNKNOWN UNPROCESSED MESSAGE: ', msg['msgType']

#        time.sleep(.1)


########################################        
#    pdb.set_trace()       
######################################## 
#    plt.draw()

    print 'DONE! (press cntl-d to exit)'
    pdb.set_trace()
    
class LMAP :
    map = []
    nodeIDs = []
    nodeTypes = []
    X = []
    Y = []
    Z = []
    nEntries = 0
    
    def __init__(self) :
        pass
#        self.map = []

    def add(self,nodeID,nodeType,x,y,z) :
        self.map.append([nodeID,nodeType,x,y,z])
        self.nodeIDs.append(nodeID)
        self.nodeTypes.append(nodeType)
        self.X.append(x)
        self.Y.append(y)
        self.Z.append(z)
        self.nEntries += 1

    def update(self,nodeID,x,y,z) : 
        index = self.nodeIDs.index(nodeID)
        self.map[index][2:] = [x, y, z]
        self.X[index] = x
        self.Y[index] = y
        self.Z[index] = z
        
    def pprint(self) :
        print 'LMAP: %d nodes\n%5s %5s %5s %9s %9s' % (self.nEntries,'NodeID','Type','X','Y','Z')
        for k in range(0,self.nEntries-1) :
            print '%5d %9s %9.3f %9.3f %9.3f' % (self.nodeIDs[k],self.nodeTypes[k],self.X[k],self.Y[k],self.Z[k])
        
    def getLoc(self,nodeID) :
        nodeIndex = self.nodeIDs.index(nodeID)
        return [self.X[nodeIndex],self.Y[nodeIndex],self.Z[nodeIndex]]
        
        
# def rolling_window(a, window):
#     shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
#     strides = a.strides + (a.strides[-1],)
#     return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
#
# def printFlatArray(label,locArray) :
#     rows,cols = locArray.shape
#     print '%s: ' % label,
#     for i in range(0,cols) :
#         print '[%.4f %.4f %.4f]' % tuple([k for k in locArray[:,i]]),
#     print '\n'

# class EKF2D :
#
#     X = []
#     P = []
#     ree = []
#     aee = []
#     zFixed = []
#     anchorLoc = []
#     t_prev = []
#     locErrEst = []
#     dt = []
#
#     def __init__(self, X=X, P=P, aee=aee, zFixed=zFixed) :
#
#         if X :
#             self.X = X
#         else :
#             self.X = np.array([0, 0, 0, 0]).T
#
#         if P :
#             self.P = P
#         else :
#             self.P = np.array([[5., 0., 0., 0.],
#                                [0., 1., 0., 0.],
#                                [0., 0., 5., 0.],
#                                [0., 0., 0., 1.]])
#         self.aee = aee
#         self.zFixed = zFixed
#
#
#     def predict(self, dt=None, aee=None) :
#
#         if aee == None :
#             aee = self.aee
#
#         F = np.array([[1., dt, 0., 0.],
#                       [0., 1., 0., 0.],
#                       [0., 0., 1., dt],
#                       [0., 0., 0., 1.]])
#         Xp = np.dot(F,self.X)
#
#         Q = np.array([[dt**4./3.,  dt**3./2.,         0.,         0.],
#                       [dt**3./2.,     dt**2.,         0.,         0.],
#                       [0.,                0.,  dt**4./3.,  dt**3./2.],
#                       [0.,                0.,  dt**3./2.,     dt**2.]]) * aee**2.
#
#         Pp = dot(dot(F,self.P),F.T) + Q
#
#         return (Xp,Pp)
#
#
#     def update(self,t,rMeas,ree,anchorLoc) :
#
#         if not self.t_prev :
#             dt = 1
#         else :
#             dt = t - self.t_prev
#         self.t_prev = t
# #        print 'dt=%.3f, rMeas=%.3f, ree=%.3f, zFixed = %.3f, Axyz=[%.3f,%.3f,%.3f]' % (dt,rMeas,ree,self.zFixed,anchorLoc[0],anchorLoc[1],anchorLoc[2])
#
#         # F is a 4x4 State Transition Matrix
#         F = np.array([[1., dt, 0., 0.],
#                       [0., 1., 0., 0.],
#                       [0., 0., 1., dt],
#                       [0., 0., 0., 1.]])
#         # print 'F:'
#         # for irow in range(0,4) :
#         #     print '[%.4f, %.4f, %.4f, %.4f]' % (F[irow][0],F[irow][1],F[irow][2],F[irow][3])
#
#         # Prepare Q using accelError
#         Q = np.array([[dt**4./3.,  dt**3./2.,         0.,         0.],
#                       [dt**3./2.,     dt**2.,         0.,         0.],
#                       [0.,                0.,  dt**4./3.,  dt**3./2.],
#                       [0.,                0.,  dt**3./2.,     dt**2.]]) * self.aee**2.
#         # print 'Q:'
#         # for irow in range(0,4) :
#         #     print '[%.4f, %.4f, %.4f, %.4f]' % (Q[irow][0],Q[irow][1],Q[irow][2],Q[irow][3])
#
#         # Update the predicted state vector and the predicted covariance matrix
#         Xp = dot(F,self.X)
# #        print 'Xp ', Xp
#
# #        Ppred = F*P*F.T + Q
#         Pp = dot(dot(F,self.P),F.T) + Q
#         # print 'Pp:'
#         # for irow in range(0,4) :
#         #     print '[%.4f, %.4f, %.4f, %.4f]' % (Pp[irow][0],Pp[irow][1],Pp[irow][2],Pp[irow][3])
#
#         (Xp,Pp) = self.predict(dt=dt)
#
#         # update the predicted range measure
# #        Xpred0 = Xpred.item(0)
# #        Xpred2 = Xpred.item(2)
# #        z = self.zFixed
#         rp = math.sqrt((Xp[0]    - anchorLoc[0])**2.
#                      + (Xp[2]    - anchorLoc[1])**2.
#                      + (self.zFixed - anchorLoc[2])**2.)
# #        print 'rp ', rp
#
#         # update the linearized measurement matrix
#         H = np.array([(Xp[0] - anchorLoc[0])/rp, 0., (Xp[2] - anchorLoc[1])/rp, 0.])
# #        print 'H ', H
# #        H = [(Xpred[0] - anchorLoc[1])/rPred, 0, (Xpred[2] - anchorLoc[1])/rPred, 0]
#
#         # Update the Kalman Gain - the normalized ratio of confidence in modeled meas vs. actual meas
# #        K = (Ppred*H.T) / (H*Ppred*H.T) + rMeasErrEst**2
#         K = dot(Pp,H.T)/(dot(dot(H,Pp),H.T) + ree**2)
# #        print 'K ', K
# #        R = rangeErrEst**2
# #        K = dot(P,H.T).dot(inv(dot(H,P).dot(H.T) + R))
#
#         # Update the State and Covariance estimates
#         innovation = rMeas - rp
#         self.X = Xp + K*innovation
#
#         self.P = dot((np.eye(4) - np.outer(K.T,H)),Pp)
#
#         self.locErrEst = abs(innovation) + math.sqrt(np.trace(self.P))
#
# #        print 'EKF X: ', self.X
#         # print 'EKF X: [%.4f, %.4f, %.4f, %.4f]' % (self.X[0],self.X[1],self.X[2],self.X[3])
#         # print 'EKF P: '
#         # for irow in range(0,4) :
#         #     print '[%.4f, %.4f, %.4f, %.4f]' % (self.P[irow][0],self.P[irow][1],self.P[irow][2],self.P[irow][3])
#         # print 'LEE  : ', self.locErrEst
# ########################################
# #        pdb.set_trace()
# ########################################
        

        
class gui2D :
    figh = []
    axh = []
    anch = []
    wph = []
    mobh = []
    txth = []
    rlineh = []
    msgIDh = []

    ancColor = 'b'
    wpColor = '.1'
#    mobColor = 'r'

    axisLimits = []
    xprev = []
    yprev = []
    rescaleFlag = []
    
    trail = []
    trailSaved = []
    nTrailPts = []
    trailLen = []
        
    def __init__(self,inputfile,axisLimits,trailLen=20,rescaleFlag=True) :
        self.trailLen = trailLen
        
        plt.ion()
        self.figh = plt.figure(figsize=(15,10))

        self.axh = self.figh.add_subplot(111)
        if axisLimits :
            self.rescaleFlag = rescaleFlag
            plt.axis(axisLimits)
            self.axisLimits = axisLimits
        else :
            self.rescaleFlag = rescaleFlag
            self.axisLimits = [np.inf, -np.inf, np.inf, -np.inf]
        
        plt.grid(True)
        plt.axis('equal')
        [pn,fn] = os.path.split(inputfile)
        plt.title('NavNet Logfile Replay: %s' % fn)        
            
    def rescaleAxis(self,x,y) :
        if not self.rescaleFlag :
            return
        if self.axisLimits[0] > x :
            self.axisLimits[0] = x
        if self.axisLimits[1] < x :
            self.axisLimits[1] = x
        if self.axisLimits[2] > y :
            self.axisLimits[2] = y
        if self.axisLimits[3] < y :
            self.axisLimits[3] = y
        plt.xlim(self.axisLimits[0]-1,self.axisLimits[1]+1)
        plt.ylim(self.axisLimits[2]-1,self.axisLimits[3]+1)
        
    # def drawMap(self,map) :
    #     for k in range(0,map.nEntries-1) :
    #         nodeID = map.nodeIDs[k]
    #         nodeType = map.nodeTypes[k]
    #         x = map.X[k]
    #         y = map.Y[k]
    #
    #         if 'Anchor' == nodeType :
    #             plt.plot(x,y,'o',markersize=8,markerfacecolor=self.ancColor,hold=True)
    #             plt.text(x,y,nodeID)
    #         elif 'Mobile' == nodeType :
    #             pltH = plt.plot(x,y,'d',markersize=8,markerfacecolor=self.mobColor,hold=True)
    #             txtH = plt.text(x,y,nodeID)
    #             self.mobH.append([nodeID,pltH,txtH])
    #         elif 'Waypoint' == nodeType :
    #             plt.plot(x,y,'o',markersize=16,markerfacecolor='none',hold=True)
    #             plt.text(x,y,nodeID)
    #         self.rescaleAxis(x,y)
    #
    #         plt.draw()

    def drawAnchor(self,nodeID,x,y,z) :
        plt.plot(x,y,'o',markersize=8,markerfacecolor=self.ancColor,hold=True)
        plt.text(x,y,nodeID,verticalalignment='top')
        self.rescaleAxis(x,y)
        plt.draw()

    def drawWaypoint(self,nodeID,x,y,z) :
        plt.plot(x,y,'o',markersize=16,markerfacecolor='none',hold=True)
#        plt.text(x,y,nodeID,verticalalignment='top')             
        self.rescaleAxis(x,y)
        plt.draw()

    def drawMobile(self,msgID,nodeID,x,y,z,color) :
        self.updateMsgID(msgID)

        if not self.mobh :
            self.xprev = x
            self.yprev = y
            self.trail, = plt.plot([x,x],[y,y],'.',color='0.5')
            self.mobh, = plt.plot(x,y,'d',markersize=8,markerfacecolor=color,hold=True)
            self.txth = plt.text(x,y,nodeID,verticalalignment='top')
            self.trailSaved = np.array([[x,y]])
            self.nTrailPts = 1
            
        else :
            self.nTrailPts += 1
            if self.nTrailPts < self.trailLen :
                self.trailSaved = np.append(self.trailSaved,[[x,y]],axis=0)
            else :
                self.trailSaved[0:-1,:] = self.trailSaved[1:]
                self.trailSaved[-1] = [x,y]
            self.trail.set_xdata(self.trailSaved[:,0])
            self.trail.set_ydata(self.trailSaved[:,1])
#            self.trail.set_color(color)

            self.mobh.set_xdata(x)
            self.mobh.set_ydata(y)
            self.mobh.set_markerfacecolor(color)

            self.txth.set_x(x)
            self.txth.set_y(y)
            self.xprev = x
            self.yprev = y
            plt.draw()
########################################        
#            pdb.set_trace()       
########################################
        
    def markWaypoints(self,wpList,map) :
        for iWP in range(0,len(wpList)) :
            wpIndex = map.nodeIDs.index(wpList[iWP])
            nodeID = map.nodeIDs[wpIndex]
            x = map.X[wpIndex]
            y = map.Y[wpIndex]
#            z = map.Z[iWP]
            plt.plot(x,y,'o',markersize=20,linewidth=5,markerfacecolor='g',hold=True)
            plt.plot(x,y,'o',markersize=15,linewidth=5,markerfacecolor='w',hold=True)
            plt.text(x-.03,y-.06,nodeID)             
        plt.draw()
        
    def drawLine(self,loc1,loc2) :
        plt.plot([loc1[0],loc2[0]],[loc1[1],loc2[1]],'g:',linewidth=2,hold=True)

    def drawRangeLine(self,fromID,toID,map) :
        fromLoc = map.getLoc(fromID)
        toLoc = map.getLoc(toID)
        if not self.rlineh :
            self.rlineh, = self.axh.plot([fromLoc[0],toLoc[0]],[fromLoc[1],toLoc[1]],linestyle='--',color='0.5')
        else :
            self.rlineh.set_xdata([fromLoc[0],toLoc[0]])
            self.rlineh.set_ydata([fromLoc[1],toLoc[1]])
        plt.draw()
        
    def updateMsgID(self,msgID) :
        xmin,xmax = self.axh.get_xlim()
        ymin,ymax = self.axh.get_ylim()
        if not self.msgIDh :
            self.msgIDh = plt.text(xmax-.1,ymax-.1,('MsgID: %d' % msgID),ha='right',va='top',fontsize=14)
        else :
            self.msgIDh.set_text(('MsgID: %d' % msgID))

        plt.draw()
        

def queryUser() :
    root = Tkinter.Tk()
    root.withdraw() # don't want a full GUI
    root.update()
    ffn = tkFileDialog.askopenfilename(parent=root,title="Choose an input file",filetypes=[("Log files","*.csv")])
    return ffn
    
class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()
    
        
if __name__ == "__main__" :
    main(sys.argv[1:])


########################################        
#        pdb.set_trace()       
########################################   

