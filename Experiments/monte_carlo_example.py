import numpy as np
from numpy.random import multivariate_normal
from nonlinear_plots import plot_monte_carlo_mean
import pdb
import matplotlib.pyplot as plt


def f(x,y):
    return x+y, .1*x**2 + y*y
   
mean = (0, 0)
p = np.array([[32, 15], [15., 40.]])

# Compute linearized mean
mean_fx = f(*mean)

#generate random points
xs, ys = multivariate_normal(mean=mean, cov=p, size=10000).T
plt.ion()
plot_monte_carlo_mean(xs, ys, f, mean_fx, 'Linearized Mean');
########################################        
pdb.set_trace()       
########################################
