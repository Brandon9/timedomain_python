from filterpy.gh import GHFilter
f = GHFilter (x=0., dx=0., dt=1., g=.8, h=.2)

f.update(z=1.2)

print(f.x, f.dx)

print(f.update(z=2.1, g=.85, h=.15))

print(f.batch_filter([3., 4., 5.]))

