
from filterpy.kalman import unscented_transform
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
import scipy.stats as stats

#from filterpy.monte_carlo import stratified_resample
#import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import randn, random, uniform, multivariate_normal, seed
#import scipy.stats
#from RobotLocalizationParticleFilter import *

from nonlinear_plots import plot_monte_carlo_mean




# def f(x,y):
#     return x+y, .1*x**2 + y*y

def f_nonlinear_xy(x, y):
    return np.array([x + y, .1*x**2 + y*y])

#initial mean and covariance
mean = (0, 0)
p = np.array([[32., 15],
              [15., 40.]])

### create sigma points - we will learn about this later
### in the chapter
points = SigmaPoints(n=2, alpha=.1, beta=2., kappa=1.)
Wm, Wc = points.weights()
sigmas = points.sigma_points(mean, p)

### pass through nonlinear function
sigmas_f = np.zeros((5, 2))
for i in range(5):
    sigmas_f[i] = f_nonlinear_xy(sigmas[i, 0], sigmas[i ,1])

### use unscented transform to get new mean and covariance
ukf_mean, ukf_cov = unscented_transform(sigmas_f, Wm, Wc)

#generate random points
np.random.seed(100)
xs, ys = multivariate_normal(mean=mean, cov=p, size=5000).T


f = f_nonlinear_xy
plot_monte_carlo_mean(xs, ys, f, ukf_mean, 'Unscented Mean')
plt.subplot(121)
plt.scatter(sigmas[:,0], sigmas[:,1], c='r', s=40);
