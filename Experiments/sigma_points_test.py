import matplotlib.pyplot as plt
from nonlinear_plots import plot_nonlinear_func
from numpy.random import normal
import numpy as np

gaussian=(0., 1.)
data = normal(loc=gaussian[0], scale=gaussian[1], size=500000)

def g(x):
    return (np.cos(4*(x/2 + 0.7))) - 1.3*x

#with figsize(y=5):
plot_nonlinear_func(data, g, gaussian=gaussian)