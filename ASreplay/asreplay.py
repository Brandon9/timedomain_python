#!/usr/local/bin/python
import sys
sys.path.append('/Users/Brandon1/Workspace/Python')
from TimeDomain.utilities import parseLogMsg
import pdb
import os
#import math
import numpy as np
#import itertools
if os.name == 'posix' :
    import matplotlib
    matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import Tkinter#, tkFileDialog
import time
#import pylab


locResidual_saved = []
def main() :
    data_folder = '/Volumes/Transcend/Data/AStest'
    data_fn = 'AutoSurvey_P440_4Node_001.csv'
    ffn_in = os.path.join(data_folder,data_fn)
#    fullfile = []  # use this to query the user
    
    if not ffn_in :  # if empty
        ffn_in = queryUser()
    
    ilocRes = 0  
    with open(ffn_in,'r') as f:
        iline = 0
        irange = 0
        for line in f:
            iline += 1
            print '\n%d: %s' % (iline,line[0:-1])
#            if (iline > 80) and (iline < 370) :
#                continue

            msg = parseLogMsg(line)
            if not msg : # msg is empty
                continue
            
            # Get rid of blank lines
            if len(line) < 5 :
                continue
                
            if 'AsConfig' == msg['msgType'] :
                asConfig = msg
                asv = Autosurvey()
                nNodes = msg['NodeCount']

                nodeIdList = msg['NodeIDs']
                orientations = [msg['xAxis_Sign'],msg['yAxis_Sign']]
                reeThresh = msg['PRME_Thresh']
                alphaFilter = msg['AlphaFilter']
                asv.updateConfig(msg['Timestamp'],nNodes,nodeIdList,orientations,reeThresh,alphaFilter)
                gui = gui2D(ffn_in,nNodes)
                

            elif 'AsRawNodes' == msg['msgType'] :
                locArray = np.array(msg['AsRawNodes'])
                print 'AsRawNodes: ', ['%.4f' % i for i in locArray]
                print 'pyLocRaw: ', ['%.4f' % i for i in asv.locRaw.transpose().flatten()]                
                locArray = np.reshape(locArray,(asv.nodeCount,3)).T

                gui.updateLocRaw(locArray)

                # If first then initialize python version 
                if np.isnan(asv.locRaw).any() :
                    asv.initRawLoc(msg['Timestamp'],locArray)
                    gui.plotNodeLabels(asv)
                # if np.isnan(asv.locFilt).any() :
                #     asv.updateFiltLoc(msg['Timestamp'],locArray)
 
            elif 'AsFilteredNodes' == msg['msgType'] :
                asFiltArray = np.array(msg['AsFilteredArray'])
                
                print 'AsFilteredNodes: ', ['%.4f' % i for i in asFiltArray]
                pyFiltArray = asv.locFilt.transpose().flatten()
                print 'pyLocFilt:       ', ['%.4f' % i for i in pyFiltArray]
                diff = asFiltArray - pyFiltArray
                print 'diff:  ', ['%.4f' % i for i in diff]
                locArray = np.reshape(asFiltArray,(asv.nodeCount,3)).transpose()
                gui.updateLocFilt(locArray)

            elif 'AsRangeList' == msg['msgType'] :
                asRangeList = np.array(msg['AsRangeList']).tolist()
                print 'AsRangeList:', asRangeList
                print 'pyRangeList:', zip(asv.rangeList.tolist(),asv.reeList.tolist())
                
            elif 'AsWeights' == msg['msgType'] :
                print 'AsWeights: ',['%.4f' % i for i in msg['AsWeights']]
                print 'pyWeights: ',['%.4f' % i for i in asv.rWeights]
    
            elif 'AsLocResidual' == msg['msgType'] :
                print 'AsLocResidual: %.3f' % (msg['LocResidual'])
                ilocRes += 1
                if np.isnan(asv.locResidual) :
                    print 'Not yet filtering'
                    continue
                    
                print 'PyLocResidual: %.3f' % asv.locResidual
                gui.printLocResiduals(msg['LocResidual'],asv.locResidual)
                locResidual_saved.extend([msg['LocResidual']])
#                asv.locResThresh = [msg['LocResidualThreshold']]  # this should be put in config
                
                
            elif 'RcmEchoedRangeInfo' == msg['msgType'] :
                rangeInfo = msg
                irange += 1
                # If Autosurvey object has been created then update range list """
                if 'asv' in locals() :
                    asv.updateRangeInfo(msg['Timestamp'],msg['MessageID'],msg['RequesterID'],msg['ResponderID'],
                                        msg['PrecisionRange'],msg['PrecisionRangeErrEst'],msg['LEDFlags'])
                    gui.updateAsPy(asv)
                
                    
#            if iline == 370 :
# #            if irange == 20 :
#                break

    gui.plotNodeLabels(asv)

    # This is the after-plot of locResidual_saved
    fig1 = plt.figure(1)
    ax1 = fig1.add_subplot(111)
    
    locRes_saved_py = asv.locResidual_saved[1:]
    locRes_saved_me = locResidual_saved[1:]
    
    ax1.plot(locRes_saved_py,color='0.5')
    ax1.plot(locRes_saved_me,color='r')
    ax1.grid(True)
    my_xlim = ax1.get_xlim()
    my_ylim = ax1.get_ylim()
    plt.title('locResidual vs. update')

    tmp_str = 'Min: %.4f\nMax: %.4f\nFinal: %.4f'\
         % (min(locRes_saved_py),max(locRes_saved_py),locRes_saved_py[-1])

    plt.text(my_xlim[1]/2,my_ylim[1]/2,tmp_str)
    
#    plt.draw()

    print 'DONE!'
    pdb.set_trace()
    
    
def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def printFlatArray(label,locArray) :
    rows,cols = locArray.shape
    print '%s: ' % label,
    for i in range(0,cols) :
        print '[%.4f %.4f %.4f]' % tuple([k for k in locArray[:,i]]),
    print '\n'    
    
class Autosurvey :
    # Config
    nlsMaxResidual = 0.005
    nlsMaxIter = 12
    nodeCount = []
    nodeIdList = []
    xaxis_sign = []
    yaxis_sign = []
    reeThresh = []
    alphaFilter = []
    
    timestamp = []  # latest timestamp
    
    rangeList = []#np.zeros(3)
    reeList = []#np.zeros(3)
    rWeights = []#np.zeros(3)
    
    locRaw = []#np.zeros((3,3))
    locFilt = []#np.zeros((3,3))
    indexMap3 = np.array([[np.nan,0,1],[0,np.nan,2],[1,2,np.nan]])
    indexMap4 = np.array([[np.nan,0,1,2],[0,np.nan,3,4],[1,3,np.nan,5],[2,4,5,np.nan]])

    locResidual = []
    locResidual_saved = []
    sum_lr = []
    sum_lrsq = []
    n = []
    var_lr_saved = []
    z4float = False
#    z4float = True  # experimental
    
    def __init__(self) :
        print '  ASV created'

    def updateConfig(self, timestamp, nodeCount, nodeIdList, orientations, reeThresh, alphaFilter) :
        self.timestamp = timestamp
        self.nodeCount = nodeCount
        linkCount = nodeCount*(nodeCount-1)/2
        self.nodeIdList = nodeIdList
        self.xaxis_sign = orientations[0]
        self.yaxis_sign = orientations[1]
        self.reeThresh = reeThresh
        self.reeThresh = 0.130
        self.alphaFilter = alphaFilter
        
        self.rangeList = np.zeros(linkCount)
        self.rangeList[:] = np.nan
        self.reeList = np.zeros(linkCount)
        self.reeList[:] = np.nan

        self.locRaw = np.zeros((3,nodeCount))
        self.locRaw[:] = np.nan
        self.locFilt = np.zeros((3,nodeCount))
        self.locFilt[:] = np.nan
        
        print '  ASPY config: %d nodes, ' % nodeCount,
        print ' nodeIdList = ', nodeIdList
        print '  orientations:', orientations,
        print ' reeThresh = %5.3f, alphaFilter = %3.2f' % (reeThresh,alphaFilter)     
        
    def initRawLoc(self,timestamp,locArray) :
        self.timestamp = timestamp
        self.locRaw = locArray
        print '  locRaw updated: ',self.locRaw.transpose().tolist()
 
    def forceOrientation(self,locEst) :
        if (self.xaxis_sign == 1 and locEst[0,1] < 0) or (self.xaxis_sign == -1 and locEst[0,1]) > 0 :
            locEst[0,1] = -locEst[0,1]
        if (self.yaxis_sign == 1 and locEst[1,2] < 0) or (self.yaxis_sign == -1 and locEst[1,2]) > 0 :
            locEst[1,2] = -locEst[1,2]
        return locEst        
 
    def updateRangeInfo(self,timestamp,msgId,reqId,rspId,rmeas,ree,ledFlags) :
        self.timestamp = timestamp
        self.rangeInfo = [reqId,rspId,rmeas,ree,ledFlags] # [reqId,rspId,rangeMeas,ree,ledFlags]
        
        # This updates the post-processed range list
        if ree > self.reeThresh :
            print '  Range dropped: ree (%.3f) > reeThresh (%.3f)' % (ree,self.reeThresh)
########################################        
#            pdb.set_trace()       
########################################         
            return
            
        # Get rid of weird zero range error on P330s
        if rmeas == 0 :
            print '  Range measurement = 0, dropped'
            return
        
        if not reqId in self.nodeIdList :
            print '  ReqID %d not in anchor list' % reqId
            return
        if not rspId in self.nodeIdList :
            print '  RspID %d not in anchor list' % rspId
            return
        reqI = self.nodeIdList.index(reqId)
        rspI = self.nodeIdList.index(rspId)
            
        rangeList_prev = self.rangeList
        reeList_prev = self.reeList
        
        if self.nodeCount == 3 :
            indexMap = self.indexMap3
        else :
            indexMap = self.indexMap4
        rangeListIndex = int(indexMap[reqI,rspI])

        self.rangeList[rangeListIndex] = rmeas
        self.reeList[rangeListIndex] = ree
        if any(np.isnan(self.rangeList)) :
            print '  Not enough ranges yet. rangeList =', self.rangeList
            return
            
        self.rWeights = 1./self.reeList
        # Check for filter start
        if np.isnan(self.locFilt).any() :
            locSeed = self.locRaw
        else :
            locSeed = self.locFilt
        
        if self.nodeCount == 3 :
            locEst,nlsResidual,kMax = NLS_3x3(locSeed,self.rangeList,self.rWeights,self.nlsMaxResidual,self.nlsMaxIter)
        elif self.z4float == False :
            locEst,nlsResidual,kMax = NLS_4x6(locSeed,self.rangeList,self.rWeights,self.nlsMaxResidual,self.nlsMaxIter)
        else : # zfloat = True
            locEst,nlsResidual,kMax = NLS_4x6_z4float(self.locSeed,self.rangeList,self.rWeights,self.nlsMaxResidual,self.nlsMaxIter)
        
        if (kMax > self.nlsMaxIter) or (nlsResidual > self.nlsMaxResidual) :
            print "  Bad NLS solution: nlsResidual = %f, kMax = %d" % (nlsResidual,kMax)
            self.rangeList = rangeList_prev
            self.reeList = reeList_prev
########################################        
#            pdb.set_trace()       
########################################
            
        else :
            locRaw_prev = self.locRaw
            locFilt_prev = self.locFilt
            locResidual_prev = self.locResidual
            
            locEst = self.forceOrientation(locEst)
            self.locRaw = locEst

            # First time filter
            if np.isnan(self.locFilt).any() :
                self.locFilt = locEst
            
            self.locFilt = self.alphaFilter*locEst + (1-self.alphaFilter)*self.locFilt

            self.locResidual = np.sum(np.abs(locEst-self.locFilt))

            locResidualThreshold = 1000.0
            if self.locResidual > locResidualThreshold :
                print '*** Failed locResidual test locResidual = %.4f, locResidualThreshold = %.2f' % (self.locResidual,locResidualThreshold)
########################################        
#                pdb.set_trace()       
########################################
                self.rangeList = rangeList_prev
                self.reeList = reeList_prev
                self.locRaw = locRaw_prev
                self.locFilt = locFilt_prev
                self.locResidual = locResidual_prev
            else :
                self.locResidual_saved.extend([self.locResidual])
                print '*** NLS success',
                print '  rangeList: ', self.rangeList,
                print 'kMax = %d, nlsResidual = %7.5f, locResidual = %7.5f' % (kMax,nlsResidual,self.locResidual)
                printFlatArray('pyLocFilt',self.locFilt)
        
class gui2D :
    figh = []
    axh = []
    rawh = []
    pyh = []
    rawColor = 'b'
    filtColor = 'r'
    pyColor = '.5'
    filth = []
    xmax = -10
    ymax = -10
    xmin = 10
    ymin = 10
    locResTxt = []

    def __init__(self,ffn_in,nNodes) :
        plt.ion()
        self.figh = plt.figure(0)
        mgr = plt.get_current_fig_manager()
        mgr.full_screen_toggle()
        self.axh = self.figh.add_subplot(111)
        nVertices = nNodes + 1
        # xinit = [0]*nVertices
        # yinit = [0]*nVertices
        self.pyh, = plt.plot([0]*nVertices,[0]*nVertices,'o',markersize=5,markerfacecolor=self.pyColor,hold=True)
        self.filth, = plt.plot([0]*nVertices,[0]*nVertices,'ko',markersize=6,markerfacecolor='k',hold=True)
        self.rawh, = plt.plot([0]*nVertices,[0]*nVertices,'--',color=self.rawColor)
        plt.axis([-1,10,-10,10])
        
        plt.grid(True)
        [pn,fn] = os.path.split(ffn_in)
        plt.title('Autosurvey Logfile Replay: %s' % fn)
        
    def rescaleAxis(self,xvec,yvec) :
        if self.xmax < max(xvec) :
            self.xmax = max(xvec)
        if self.xmin > min(xvec) :
            self.xmin = min(xvec)
        if self.ymax < max(yvec) :
            self.ymax = max(yvec)
        if self.ymin > min(yvec) :
            self.ymin = min(yvec)
        plt.xlim(self.xmin-1,self.xmax+1)
        plt.ylim(self.ymin-1,self.ymax+1)
        
    def updateLocRaw(self,locArray) :
        self.rawh.set_xdata(np.append(locArray[0],locArray[0][0]))
        self.rawh.set_ydata(np.append(locArray[1],locArray[1][0]))
        self.rescaleAxis(locArray[0],locArray[1])
        plt.draw()

    def updateLocFilt(self,locArray) :
        plt.plot(locArray[0],locArray[1],'ko',markersize=6,markerfacecolor=self.filtColor)
        self.rescaleAxis(locArray[0],locArray[1])
        plt.draw()
    
    def updateAsPy(self,asv) :
        plt.plot(asv.locFilt[0],asv.locFilt[1],'ko',markersize=5,markerfacecolor=self.pyColor)
        self.rescaleAxis(asv.locFilt[0],asv.locFilt[1])
        plt.draw()
        
    def plotNodeLabels(self,asv) :        
        for i in range(0,asv.nodeCount) :
            x = asv.locRaw[0,i]
            y = asv.locRaw[1,i]
            plt.text(x,y,str(asv.nodeIdList[i]))
            
    def printLocResiduals(self,locResidual,pyLocResidual) :
        if not self.locResTxt :
            self.locResTxt = plt.text(self.xmax-.1,self.ymax-.1,
                ('AsLocResidual: %.4f\nPyLocResidual: %.4f' % (locResidual,pyLocResidual)),
                ha='right',va='top',fontsize=14)
        else :
            self.locResTxt.set_text(('AsLocResidual: %.4f\nAsLocResidual_Py: %.4f' % (locResidual,pyLocResidual)))
        

def NLS_3x3(locSeed,rangeList,rWeights,nlsMaxResidual,nlsMaxIter) :
    #print 'NLS_3x3'
    x1 = locSeed[0,0]; x2 = locSeed[0,1]; x3 = locSeed[0,2]
    y1 = locSeed[1,0]; y2 = locSeed[1,1]; y3 = locSeed[1,2]
    z1 = locSeed[2,0]; z2 = locSeed[2,1]; z3 = locSeed[2,2]

    for k in range(0,nlsMaxIter) :
        f12 = (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 - rangeList[0]**2
        f13 = (x1-x3)**2 + (y1-y3)**2 + (z1-z3)**2 - rangeList[1]**2
        f23 = (x2-x3)**2 + (y2-y3)**2 + (z2-z3)**2 - rangeList[2]**2
    
        F = np.array([[f12],[f13],[f23]])
    
        df12dx2=-2*(x1-x2); df12dx3= 0;         df12dy3= 0;
        df13dx2= 0;         df13dx3=-2*(x1-x3); df13dy3=-2*(y1-y3);
        df23dx2=+2*(x2-x3); df23dx3=-2*(x2-x3); df23dy3=-2*(y2-y3);
    
        J = np.matrix([[df12dx2, df12dx3, df12dy3],
                       [df13dx2, df13dx3, df13dy3], 
                       [df23dx2, df23dx3, df23dy3]])
    
        H = ((J.transpose()*np.diag(rWeights)*J)**-1)*J.transpose()*np.diag(rWeights)*F
    
        x2 = x2 - H[0].item()
        x3 = x3 - H[1].item()
        y3 = y3 - H[2].item()
    
        nlsResidual = np.linalg.norm(H)
        if nlsResidual < nlsMaxResidual :
            break
    
    locArray = np.array([[x1,x2,x3],[y1,y2,y3],[z1,z2,z3]])
    return locArray, nlsResidual, k
        

def NLS_4x6(locSeed,rangeList,rWeights,nlsMaxResidual,nlsMaxIter) :
    #print 'NLS_3x3'
    x1 = locSeed[0,0]; x2 = locSeed[0,1]; x3 = locSeed[0,2]; x4 = locSeed[0,3]
    y1 = locSeed[1,0]; y2 = locSeed[1,1]; y3 = locSeed[1,2]; y4 = locSeed[1,3]
    z1 = locSeed[2,0]; z2 = locSeed[2,1]; z3 = locSeed[2,2]; z4 = locSeed[2,3]

    for k in range(0,nlsMaxIter) :
        f12 = (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 - rangeList[0]**2
        f13 = (x1-x3)**2 + (y1-y3)**2 + (z1-z3)**2 - rangeList[1]**2
        f14 = (x1-x4)**2 + (y1-y4)**2 + (z1-z4)**2 - rangeList[2]**2
        f23 = (x2-x3)**2 + (y2-y3)**2 + (z2-z3)**2 - rangeList[3]**2
        f24 = (x2-x4)**2 + (y2-y4)**2 + (z2-z4)**2 - rangeList[4]**2
        f34 = (x3-x4)**2 + (y3-y4)**2 + (z3-z4)**2 - rangeList[5]**2        
    
        F = np.array([[f12],[f13],[f14],[f23],[f24],[f34]])
    
        #    x2                  x3                    x4                    y3                 y4
        df12dx2 = -2*(x1-x2); df12dx3 =  0;         df12dx4 =  0;         df12dy3 =  0;         df12dy4 =  0         # f12
        df13dx2 =  0;         df13dx3 = -2*(x1-x3); df13dx4 =  0;         df13dy3 = -2*(y1-y3); df13dy4 =  0         # f13
        df14dx2 =  0;         df14dx3 =  0;         df14dx4 = -2*(x1-x4); df14dy3 =  0;         df14dy4 = -2*(y1-y4) # f14
        df23dx2 = +2*(x2-x3); df23dx3 = -2*(x2-x3); df23dx4 =  0;         df23dy3 = -2*(y2-y3); df23dy4 =  0         # f23
        df24dx2 = +2*(x2-x4); df24dx3 =  0;         df24dx4 = -2*(x2-x4); df24dy3 =  0;         df24dy4 = -2*(y2-y4) # f24
        df34dx2 =  0;         df34dx3 = +2*(x3-x4); df34dx4 = -2*(x3-x4); df34dy3 = +2*(y3-y4); df34dy4 = -2*(y3-y4) # f34
    
        J = np.matrix([[df12dx2, df12dx3, df12dx4, df12dy3, df12dy4],
                       [df13dx2, df13dx3, df13dx4, df13dy3, df13dy4],
                       [df14dx2, df14dx3, df14dx4, df14dy3, df14dy4],
                       [df23dx2, df23dx3, df23dx4, df23dy3, df23dy4],
                       [df24dx2, df24dx3, df24dx4, df24dy3, df24dy4],
                       [df34dx2, df34dx3, df34dx4, df34dy3, df34dy4]])
                       
        H = ((J.transpose()*np.diag(rWeights)*J)**-1)*J.transpose()*np.diag(rWeights)*F
    
        x2 = x2 - H[0].item()
        x3 = x3 - H[1].item()
        x4 = x4 - H[2].item()
        y3 = y3 - H[3].item()
        y4 = y4 - H[4].item()
    
        nlsResidual = np.linalg.norm(H)
        if nlsResidual < nlsMaxResidual :
            break
    
    locArray = np.array([[x1,x2,x3,x4],[y1,y2,y3,y4],[z1,z2,z3,z4]])
    return locArray, nlsResidual, k
    
def NLS_4x6_z4float(locSeed,rangeList,rWeights,nlsMaxResidual,nlsMaxIter) :
    # this one is supposed to allow z4 to float.  z4 cannot == 0
    x1 = locSeed[0,0]; x2 = locSeed[0,1]; x3 = locSeed[0,2]; x4 = locSeed[0,3]
    y1 = locSeed[1,0]; y2 = locSeed[1,1]; y3 = locSeed[1,2]; y4 = locSeed[1,3]
    z1 = locSeed[2,0]; z2 = locSeed[2,1]; z3 = locSeed[2,2]; z4 = locSeed[2,3]

    for k in range(0,nlsMaxIter) :
        f12 = (x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2 - rangeList[0]**2
        f13 = (x1-x3)**2 + (y1-y3)**2 + (z1-z3)**2 - rangeList[1]**2
        f14 = (x1-x4)**2 + (y1-y4)**2 + (z1-z4)**2 - rangeList[2]**2
        f23 = (x2-x3)**2 + (y2-y3)**2 + (z2-z3)**2 - rangeList[3]**2
        f24 = (x2-x4)**2 + (y2-y4)**2 + (z2-z4)**2 - rangeList[4]**2
        f34 = (x3-x4)**2 + (y3-y4)**2 + (z3-z4)**2 - rangeList[5]**2        
    
        F = np.array([[f12],[f13],[f14],[f23],[f24],[f34]])
    
        #  x2                  x3                    x4                    y3                    y4                    z4
        df12dx2 = -2*(x1-x2); df12dx3 =  0;         df12dx4 =  0;         df12dy3 =  0;         df12dy4 =  0;         df12dz4 = 0;          # f12
        df13dx2 =  0;         df13dx3 = -2*(x1-x3); df13dx4 =  0;         df13dy3 = -2*(y1-y3); df13dy4 =  0;         df13dz4 = 0;          # f13
        df14dx2 =  0;         df14dx3 =  0;         df14dx4 = -2*(x1-x4); df14dy3 =  0;         df14dy4 = -2*(y1-y4); df14dz4 = -2*(z1-z4); # f14
        df23dx2 = +2*(x2-x3); df23dx3 = -2*(x2-x3); df23dx4 =  0;         df23dy3 = -2*(y2-y3); df23dy4 =  0;         df23dz4 = 0;          # f23
        df24dx2 = +2*(x2-x4); df24dx3 =  0;         df24dx4 = -2*(x2-x4); df24dy3 =  0;         df24dy4 = -2*(y2-y4); df24dz4 = -2*(z2-z4); # f24
        df34dx2 =  0;         df34dx3 = +2*(x3-x4); df34dx4 = -2*(x3-x4); df34dy3 = +2*(y3-y4); df34dy4 = -2*(y3-y4); df34dz4 = -2*(z3-z4); # f34
    
        J = np.matrix([[df12dx2, df12dx3, df12dx4, df12dy3, df12dy4, df12dz4],
                       [df13dx2, df13dx3, df13dx4, df13dy3, df13dy4, df13dz4],
                       [df14dx2, df14dx3, df14dx4, df14dy3, df14dy4, df14dz4],
                       [df23dx2, df23dx3, df23dx4, df23dy3, df23dy4, df23dz4],
                       [df24dx2, df24dx3, df24dx4, df24dy3, df24dy4, df24dz4],
                       [df34dx2, df34dx3, df34dx4, df34dy3, df34dy4, df34dz4]])
                       
        H = ((J.transpose()*np.diag(rWeights)*J)**-1)*J.transpose()*np.diag(rWeights)*F
    
        x2 = x2 - H[0].item()
        x3 = x3 - H[1].item()
        x4 = x4 - H[2].item()
        y3 = y3 - H[3].item()
        y4 = y4 - H[4].item()
        z4 = z4 - H[5].item()
    
        nlsResidual = np.linalg.norm(H)
        if nlsResidual < nlsMaxResidual :
            break
    
    locArray = np.array([[x1,x2,x3,x4],[y1,y2,y3,y4],[z1,z2,z3,z4]])
    return locArray, nlsResidual, k 
    
def queryUser() :
    root = Tkinter.Tk()
    root.withdraw() # don't want a full GUI
    root.update()
    ffn = tkFileDialog.askopenfilename(parent=root,title="Choose an input file",filetypes=[("Log files","*.csv")])
    return ffn
        
if __name__ == "__main__" :
    main()



