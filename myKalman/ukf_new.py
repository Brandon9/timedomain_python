from math import tan, sin, cos, sqrt, atan2, pi
import numpy as np
import matplotlib.pyplot as plt
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.stats import plot_covariance_ellipse
from numpy.random import randn
import pdb


def move(t):
    A = 10
#    x = A*cos(2*pi*t-pi/2)
    x = 5*t
    y = A*sin(2*pi*t)
    y = 5*t
    return (x,y)
    
def fx2(X,dt) :
    F = np.array([[1., dt, 0., 0.],
                  [0., 1., 0., 0.],
                  [0., 0., 1., dt],
                  [0., 0., 0., 1.]])
    Xp = np.dot(F,X)
    return Xp
    
def hx2(X,refLoc) :
    x, y = X[0], X[2]
    x0, y0 = refLoc[0], refLoc[1]
    d = sqrt((x-x0)**2 + (y-y0)**2)
    return d

plt.ion()    

anchorMap = np.array([[-1, -1], [0, 2]])
plt.scatter(anchorMap[:, 0], anchorMap[:, 1], marker='d', s=30)


dt = .1
sigma_range=0.01
points2 = SigmaPoints(n=4, alpha=.00001, beta=2, kappa=0)
ukf2 = UKF(dim_x=4, dim_z=1, fx=fx2, hx=hx2, dt=dt, points=points2)
ukf2.x = np.array([0, 0, 0, 0])
ukf2.P = np.diag([.1, .1, .1, .1])
ukf2.R = np.diag([sigma_range**2])
ukf2.Q = np.eye(4)*0.0001

    
track = []
for k in range(0,20) :
    t = k/32.     
    (sim_pos) = move(t) 
    track.append(sim_pos)
    curLoc = np.array(track[-1])
    plt.plot(curLoc[0],curLoc[1],'r*',hold='True')
    # if not trackh :
    #     print 'not trackh'
    #     curLoc = np.array(track[-1])
    #     trackh = plt.plot(curLoc[0],curLoc[1],'r*',hold='True')
    # else
    #     plt.
    plt.draw()

    ukf2.predict()
    plot_covariance_ellipse((ukf2.x[0], ukf2.x[2]), ukf2.P[::2,::2],std=6,facecolor='b', alpha=0.08)

    anchorI = 1
    if 0 == k % 2 :
        anchorI = 0
    anchorLoc = anchorMap[anchorI]    
    x, y = sim_pos[0], sim_pos[1]    
    dx, dy = anchorLoc[0] - x, anchorLoc[1] - y
    z2 = sqrt(dx**2 + dy**2) + randn()*sigma_range
    
    ukf2.update(z2, hx_args=(tuple(anchorLoc),), R=np.array(sigma_range**2))

    plot_covariance_ellipse((ukf2.x[0], ukf2.x[2]), ukf2.P[::2,::2],std=6,facecolor='g', alpha=0.4)
########################################        
    pdb.set_trace()       
########################################

track = np.array(track)
#plt.plot(track[:, 0],track[:,1],'o--',color='0.4',lw=2,markersize=8)
plt.axis('equal')
plt.grid()
plt.title("UKF localization")
print('final covariance', ukf2.P.diagonal())

########################################        
pdb.set_trace()       
########################################

#plt.show()

  



