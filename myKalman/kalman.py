#!/usr/local/bin/python

import pdb
import numpy as np
from numpy import dot
from math import sqrt

########################################        
#        pdb.set_trace()       
########################################

class EKF2D :

    X = np.zeros(4)
    P = np.array([[5., 0., 0., 0.],
                  [0., 1., 0., 0.],
                  [0., 0., 5., 0.], 
                  [0., 0., 0., 1.]])
    aee = 0.01
    zFixed = 0.0

    t_prev = []
    locErrEst = []

    def __init__(self,t,X,P,aee,zFixed) :

        if isinstance(t,float) :
            self.t_prev = t
            
        if type(X) is np.ndarray :
            self.X = X
            
        if type(P) is np.ndarray :
            self.P = P

        if isinstance(aee,float) :
            self.aee = aee
        
        if isinstance(zFixed,float) :
            self.zFixed = zFixed
            
########################################        
#        pdb.set_trace()       
########################################
    def update(self,t,rMeas,ree,anchorLoc) :

        dt = t - self.t_prev
        self.t_prev = t
#        print 'dt=%.3f, rMeas=%.3f, ree=%.3f, zFixed = %.3f, Axyz=[%.3f,%.3f,%.3f]' % (dt,rMeas,ree,self.zFixed,anchorLoc[0],anchorLoc[1],anchorLoc[2])
        
        # F is a 4x4 State Transition Matrix
        F = np.array([[1., dt, 0., 0.],
                      [0., 1., 0., 0.],
                      [0., 0., 1., dt],
                      [0., 0., 0., 1.]])
        # print 'F:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (F[irow][0],F[irow][1],F[irow][2],F[irow][3])

        # Prepare Q using accelError
        Q = np.array([[dt**4./3.,  dt**3./2.,         0.,         0.],
                      [dt**3./2.,     dt**2.,         0.,         0.],
                      [0.,                0.,  dt**4./3.,  dt**3./2.],
                      [0.,                0.,  dt**3./2.,     dt**2.]]) * self.aee**2.
        # print 'Q:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (Q[irow][0],Q[irow][1],Q[irow][2],Q[irow][3])

        # Update the predicted state vector and the predicted covariance matrix
        Xp = np.dot(F,self.X)
#        print 'Xp ', Xp
        
#        Ppred = F*P*F.T + Q
        Pp = dot(dot(F,self.P),F.T) + Q
        # print 'Pp:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (Pp[irow][0],Pp[irow][1],Pp[irow][2],Pp[irow][3])
        
        # update the predicted range measure
#        Xpred0 = Xpred.item(0)
#        Xpred2 = Xpred.item(2)
#        z = self.zFixed
        rp = sqrt((Xp[0]    - anchorLoc[0])**2.
                + (Xp[2]    - anchorLoc[1])**2.
                + (self.zFixed - anchorLoc[2])**2.)
#        print 'rp ', rp

        # update the linearized measurement matrix
        H = np.array([(Xp[0] - anchorLoc[0])/rp, 0., (Xp[2] - anchorLoc[1])/rp, 0.])
#        print 'H ', H
#        H = [(Xpred[0] - anchorLoc[1])/rPred, 0, (Xpred[2] - anchorLoc[1])/rPred, 0]

        # Update the Kalman Gain - the normalized ratio of confidence in modeled meas vs. actual meas
#        K = (Ppred*H.T) / (H*Ppred*H.T) + rMeasErrEst**2
        K = dot(Pp,H.T)/(dot(dot(H,Pp),H.T) + ree**2)
#        print 'K ', K
#        R = rangeErrEst**2
#        K = dot(P,H.T).dot(inv(dot(H,P).dot(H.T) + R))
        
        # Update the State and Covariance estimates
        innovation = rMeas - rp
        self.X = Xp + K*innovation
    
        self.P = dot((np.eye(4) - np.outer(K.T,H)),Pp)

        self.locErrEst = abs(innovation) + sqrt(np.trace(self.P))

#        print 'EKF X: ', self.X
        # print 'EKF X: [%.4f, %.4f, %.4f, %.4f]' % (self.X[0],self.X[1],self.X[2],self.X[3])
        # print 'EKF P: '
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (self.P[irow][0],self.P[irow][1],self.P[irow][2],self.P[irow][3])
        # print 'LEE  : ', self.locErrEst
        
        
class UKF2D :

    X = np.zeros(4)
    P = np.array([[5., 0., 0., 0.],
                  [0., 1., 0., 0.],
                  [0., 0., 5., 0.], 
                  [0., 0., 0., 1.]])
    aee = 0.01
    zFixed = 0.0

    t_prev = []
    locErrEst = []

    def __init__(self,t,X,P,aee,zFixed) :

        if isinstance(t,float) :
            self.t_prev = t
            
        if type(X) is np.ndarray :
            self.X = X
            
        if type(P) is np.ndarray :
            self.P = P

        if isinstance(aee,float) :
            self.aee = aee
        
        if isinstance(zFixed,float) :
            self.zFixed = zFixed
            
    def f(self,X,dt) :
        F = np.array([[1., dt, 0., 0.],
                      [0., 1., 0., 0.],
                      [0., 0., 1., dt],
                      [0., 0., 0., 1.]])
        Xp = np.dot(F,X)
        Pp = dot(dot(F,self.P),F.T) + Q
########################################        
        pdb.set_trace()       
########################################
        return (Xp,Pp)

    def h(self,

    def sigma_function(self,X,P) :
        pass
        
    def weight_function(self,n,parms) :
        pass
            
    def predict(self,X,P) :
        sigmaPts = sigmaFn(X,P)
        (Wm,Wc) = weightFn(n,parms)
        Y = f(X,dt)
        Xp = sum(Wm*Y)
        Pp = sum(Wc*(Y-X)*(Y-X).T) + Q
            
    def update(self,t,rMeas,ree,anchorLoc) :

        dt = t - self.t_prev
        self.t_prev = t
#        print 'dt=%.3f, rMeas=%.3f, ree=%.3f, zFixed = %.3f, Axyz=[%.3f,%.3f,%.3f]' % (dt,rMeas,ree,self.zFixed,anchorLoc[0],anchorLoc[1],anchorLoc[2])
        
        # F is a 4x4 State Transition Matrix
        # print 'F:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (F[irow][0],F[irow][1],F[irow][2],F[irow][3])

        # Prepare Q using accelError
        Q = np.array([[dt**4./3.,  dt**3./2.,         0.,         0.],
                      [dt**3./2.,     dt**2.,         0.,         0.],
                      [0.,                0.,  dt**4./3.,  dt**3./2.],
                      [0.,                0.,  dt**3./2.,     dt**2.]]) * self.aee**2.
        # print 'Q:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (Q[irow][0],Q[irow][1],Q[irow][2],Q[irow][3])

        # Update the predicted state vector and the predicted covariance matrix
        Xp = np.dot(F,self.X)
#        print 'Xp ', Xp
        
#        Ppred = F*P*F.T + Q
        Pp = dot(dot(F,self.P),F.T) + Q
        # print 'Pp:'
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (Pp[irow][0],Pp[irow][1],Pp[irow][2],Pp[irow][3])
        
        # update the predicted range measure
#        Xpred0 = Xpred.item(0)
#        Xpred2 = Xpred.item(2)
#        z = self.zFixed
        rp = sqrt((Xp[0]    - anchorLoc[0])**2.
                + (Xp[2]    - anchorLoc[1])**2.
                + (self.zFixed - anchorLoc[2])**2.)
#        print 'rp ', rp

        # update the linearized measurement matrix
        H = np.array([(Xp[0] - anchorLoc[0])/rp, 0., (Xp[2] - anchorLoc[1])/rp, 0.])
#        print 'H ', H
#        H = [(Xpred[0] - anchorLoc[1])/rPred, 0, (Xpred[2] - anchorLoc[1])/rPred, 0]

        # Update the Kalman Gain - the normalized ratio of confidence in modeled meas vs. actual meas
#        K = (Ppred*H.T) / (H*Ppred*H.T) + rMeasErrEst**2
        K = dot(Pp,H.T)/(dot(dot(H,Pp),H.T) + ree**2)
#        print 'K ', K
#        R = rangeErrEst**2
#        K = dot(P,H.T).dot(inv(dot(H,P).dot(H.T) + R))
        
        # Update the State and Covariance estimates
        innovation = rMeas - rp
        self.X = Xp + K*innovation
    
        self.P = dot((np.eye(4) - np.outer(K.T,H)),Pp)

        self.locErrEst = abs(innovation) + sqrt(np.trace(self.P))

#        print 'self.X: ', self.X
        # print 'self.X: [%.4f, %.4f, %.4f, %.4f]' % (self.X[0],self.X[1],self.X[2],self.X[3])
        # print 'self.P: '
        # for irow in range(0,4) :
        #     print '[%.4f, %.4f, %.4f, %.4f]' % (self.P[irow][0],self.P[irow][1],self.P[irow][2],self.P[irow][3])
        # print 'LEE  : ', self.locErrEst

        