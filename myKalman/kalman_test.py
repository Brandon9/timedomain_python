#!/usr/local/bin/python

import pdb
import numpy as np
import kalman
from kalman import EKF2D, UKF2D
import inspect


def objPrint(obj) :
    attrList = dir(obj)
    for attr in attrList :
        if attr[0] == '_' :
            continue
        value = getattr(obj,attr)
        print '   %s: ' % attr, repr(value)
        

#########################################################
t = 0.0
X = np.array([0, 0, 0, 0])
P = np.array([[5, 0, 0, 0],\
              [0, 1, 0, 0],\
              [0, 0, 5, 0],\
              [0, 0, 0, 1]])
aee = 0.1
zFixed = 0.0

ekf = EKF2D(t,X,P,aee,zFixed)
print '\nEKF t = %d:' % t
objPrint(ekf)

ukf = UKF2D(t,X,P,aee,zFixed)
print '\nUKF t = %d:' % t
objPrint(ukf)

#########################################################
t = 0.1
rMeas = 10.01
ree = 0.01
anchorLoc = [0,10,0]

ekf.update(t,rMeas,ree,anchorLoc) 
print '\nEKF t = %d:' % t
objPrint(ekf)


ukf.update(t,rMeas,ree,anchorLoc) 
print '\nUKF t = %d:' % t
objPrint(ukf)


########################################        
pdb.set_trace()       
########################################

