import numpy as np
from numpy import random
from numpy.random import randn
from numpy.linalg import norm
import math
from math import atan2
from filterpy.common import Q_discrete_white_noise
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.kalman import MerweScaledSigmaPoints as SigmaPoints
from ukf_internal import plot_radar
import pdb

class RadarStation(object):
    
    def __init__(self, pos, range_std, bearing_std):
        self.pos = np.asarray(pos)
        
        self.range_std = range_std
        self.bearing_std = bearing_std

    
    def reading_of(self, ac_pos):
        """ Returns (range, bearing) to aircraft. 
        Bearing is in radians.
        """
        
        diff = np.subtract(ac_pos, self.pos)
        rng = norm(diff)
        brg = atan2(diff[1], diff[0])
        return rng, brg


    def noisy_reading(self, ac_pos):
        """ Compute range and bearing to aircraft with 
        simulated noise"""
        
        rng, brg = self.reading_of(ac_pos)      
        rng += randn() * self.range_std
        brg += randn() * self.bearing_std 
        return rng, brg       
   
class ACSim(object):   
    def __init__(self, pos, vel, vel_std):
        self.pos = np.asarray(pos, dtype=float)
        self.vel = np.asarray(vel, dtype=float)
        self.vel_std = vel_std        
        
    def update(self, dt):
        """ Compute and returns next position. Incorporates 
        random variation invelocity. """
        
        dx = self.vel*dt + (randn() * self.vel_std) * dt      
        self.pos += dx     
        return self.pos
        

#plot_radar = imp.load_source('plot_radar','/Users/Brandon1/Kalman-and-Bayesian-Filters-in-Python/code/ukf_internal.py')

# def f_radar(x, dt):
#     """ state transition function for a constant velocity
#     aircraft with state vector [x, velocity, altitude]'"""
#     F = np.array([[1, dt, 0],
#                   [0,  1, 0],
#                   [0,  0, 1]], dtype=float)
#     return np.dot(F, x)
#
def h_radar(x):
    dx = x[0] - h_radar.radar_pos[0]
    dz = x[2] - h_radar.radar_pos[1]
    slant_range = math.sqrt(dx**2 + dz**2)
    bearing = math.atan2(dz, dx)
    return slant_range, bearing
#
# h_radar.radar_pos = (0, 0)
#
dt = 12. # 12 seconds between readings
range_std = 5 # meters
bearing_std = math.radians(0.5)
ac_pos = (0., 1000.)
# ac_vel = (100., 0.)
radar_pos = (0., 0.)
h_radar.radar_pos = radar_pos
#
# points = SigmaPoints(n=3, alpha=.1, beta=2., kappa=0.)
# kf = UKF(3, 2, dt, fx=f_radar, hx=h_radar, points=points)
#
# kf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=dt, var=0.1)
# kf.Q[2,2] = 0.1
#
# kf.R = np.diag([range_std**2, bearing_std**2])
# kf.x = np.array([0., 90., 1100.])
# kf.P = np.diag([300**2, 30**2, 150**2])
#
# random.seed(200)
pos = (0, 0)
radar = RadarStation(pos, range_std, bearing_std)
ac = ACSim(ac_pos, (100, 0), 0.02)
#
# time = np.arange(0, 360 + dt, dt)
# xs = []
# for _ in time:
#     ac.update(dt)
#     r = radar.noisy_reading(ac.pos)
#     kf.predict()
#     kf.update([r[0], r[1]])
#     xs.append(kf.x)
# plot_radar(xs, time)

########################################        
#pdb.set_trace()       
########################################

def f_cv_radar(x, dt):
    """ state transition function for a constant velocity 
    aircraft"""
    F = np.array([[1, dt, 0, 0],
                  [0,  1, 0, 0],
                  [0,  0, 1, dt],
                  [0,  0, 0, 1]], dtype=float)
    return np.dot(F, x)
    
def cv_UKF(fx, hx, R_std):
    points = SigmaPoints(n=4, alpha=.1, beta=2., kappa=-1.)
    kf = UKF(4, len(R_std), dt, fx=fx, hx=hx, points=points)

    kf.Q[0:2, 0:2] = Q_discrete_white_noise(2, dt=dt, var=0.1)
    kf.Q[2:4, 2:4] = Q_discrete_white_noise(2, dt=dt, var=0.1)
    kf.R = np.diag(R_std)
    kf.R = np.dot(kf.R, kf.R) # square to get rariance
    kf.x = np.array([0., 90., 1100., 0.])
    kf.P = np.diag([300**2, 3**2, 150**2, 3**2])
    return kf

random.seed(200)

ac = ACSim(ac_pos, (100, 0), 0.02)

kf = cv_UKF(f_cv_radar, h_radar, R_std=[range_std, bearing_std])
time = np.arange(0, 360 + dt, dt)
xs = []

for t in time:
    if t >= 60:
        ac.vel[1] = 300/60 # 300 meters/minute climb
    ac.update(dt)
    r = radar.noisy_reading(ac.pos)
    kf.predict()
    kf.update([r[0], r[1]]) 
    xs.append(kf.x)

print('Actual altitude: {:.1f}'.format(ac.pos[1]))
print('UKF altitude   : {:.1f}'.format(xs[-1][2]))
plot_radar(xs, time, plot_x=False, plot_vel=False, plot_alt=True)


########################################        
#pdb.set_trace()       
########################################
