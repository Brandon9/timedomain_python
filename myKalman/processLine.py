import pdb
def processLine(line) :
    
    linelist = line.split(",")
    if 'Timestamp' == linelist[0]: # jump over line descriptions
        return []

    elif ' RcmGetStatusInfoConfirm' == linelist[1] :
        #Timestamp, RcmGetStatusInfoConfirm, MessageID, PackageID, RcmVersion, UwbKernelVersion, FpgaVersion, SerialNumber, BoardType, PulserConfig, BITResults, Temperature(degC), Status
        #1444064949.000, RcmGetStatusInfoConfirm, 0, NN-dev-4 Oct  2 2015 18:38:36RL, 0.2.6, 2.2.51420, [2001.01.01] Rev 1, DEADBEEF, P330 Rev A, FCC, 0, 33.75, 0
        msg = dict([('msgType','RcmGetStatusInfoConfirm'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('PackageID',linelist[3][1:]),      
                    ('RcmVersion',linelist[4][1:]),
                    ('UwbKernelVersion',linelist[5][1:]),
                    ('FpgaVersion',linelist[6][1:]),
                    ('SerialNumber',linelist[7][1:]),
                    ('BoardType',linelist[8][1:]),
                    ('PulserConfig',linelist[9][1:]),
                    ('BITResults',int(linelist[10])),
                    ('temperature',float(linelist[11])),
                    ('Status',int(linelist[12])) 
                    ])
        return msg
        
    elif ' RcmConfig' == linelist[1] :   
        #Timestamp, RcmConfig, NodeId, PulseIntegrationIndex, AntennaMode, AntennaToggleFlag, CodeChannel, AntennaDelayA, AntennaDelayB, ScanInfo, DisableCRERanges, TransmitGain, ELR
        #1444409248.067, RcmConfig, 112, 7, 1, False, 5, 0, 0, 0, True, 63, False
########################################        
#        pdb.set_trace()       
########################################
        msg = dict([('msgType','RcmConfig'),
                    ('Timestamp',float(linelist[0])), 
                    ('NodeID',int(linelist[2])),
                    ('PulseIntegrationIndex',int(linelist[3])),      
                    ('AntennaMode',int(linelist[4])),
                    ('AntennaToggleFlag',bool(linelist[5])),
                    ('CodeChannel',int(linelist[6])),
                    ('AntennaDelayA',int(linelist[7])),
                    ('AntennaDelayB',int(linelist[8])),
                    ('ScanInfo',int(linelist[9])),
                    ('DisableCRERanges',bool(linelist[10])),
                    ('TransmitGain',int(linelist[11])),
                    ('ELR',bool(linelist[12][-2])),
                    ])
        return msg

    elif ' RcmP3XXConfig' == linelist[1] :   
        #Timestamp, RcmP3XXConfig, NodeId, Channel, PRF, TxPreambleLength, TxRxPreambleCode, DataRate, NsSFD, RxLNA, SmartTxPwr, AntennaDelay, RangeInfo, ScanInfo, ELR,  OTA,  Persist
        #1444064949.001, RcmP3XXConfig, 107, 2,     64,  512,              9,                850,      True,  True,  False,      112,          0,         False,    True, False, False
        msg = dict([('msgType','RcmP3XXConfig'),
                    ('Timestamp',float(linelist[0])), 
                    ('NodeID',linelist[2][1:]),
                    ('Channel',int(linelist[3])),      
                    ('PRF',int(linelist[4])),
                    ('TxPreambleLength',int(linelist[5])),
                    ('TxRxPreambleCode',linelist[6][1:]),
                    ('DataRate',int(linelist[7])),
                    ('NsSFD',bool(linelist[8])),
                    ('RxLNA',bool(linelist[9])),
                    ('SmartTxPwr',bool(linelist[10])),
                    ('AntennaDelay',int(linelist[11])),
                    ('RangeInfo',int(linelist[12])),
                    ('ScanInfo',bool(linelist[13])),
                    ('ELR',bool(linelist[14])),
                    ('OTA',bool(linelist[15])),
                    ('Persist',bool(linelist[16]))
                    ])
        return msg

    elif ' RnGetConfigConfirm' == linelist[1] :   
        #Timestamp, RnGetConfigConfirm, MessageId, MaxNeighborAgeMs, AutosendUpdateIntervalMs, AutosendType, Flags, DefaultIf, DefaultIfAddr1, DefaultIfAddr2, EmbeddedTimestamp, Status
        #1444064949.002, RnGetConfigConfirm, 9, 10000, 300, 0, 0, 0, 0, 0, 5988158, 0        
        msg = dict([('msgType','RnGetConfigConfirm'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('MaxNeighborAgeMs',int(linelist[3])),      
                    ('AutosendUpdateIntervalMs',int(linelist[4])),
                    ('AutosendType',int(linelist[5])),
                    ('Flags',int(linelist[6])),
                    ('DefaultIf',int(linelist[7])),
                    ('DefaultIfAddr1',int(linelist[8])),
                    ('DefaultIfAddr2',int(linelist[9])),
                    ('EmbeddedTimestamp',long(linelist[10])),
                    ('Status',int(linelist[11]))
                    ])
        return msg

    elif ' RnGetAlohaConfigConfirm' == linelist[1] :   
        #Timestamp, RnGetAlohaConfigConfirm, MessageId, MinTimeBetweenTxMs, MaxTimeBetweenTxMs, MaxRequestDataSize, MaxResponseDataSize, Flags, Status
        #1444064949.003, RnGetAlohaConfigConfirm, 13, 10, 1990, 0, 0, 4, 0       
        msg = dict([('msgType','RnGetAlohaConfigConfirm'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('MinTimeBetweenTxMs',int(linelist[3])),      
                    ('MaxTimeBetweenTxMs',int(linelist[4])),
                    ('MaxRequestDataSize',int(linelist[5])),
                    ('MaxResponseDataSize',int(linelist[6])),
                    ('Flags',int(linelist[7])),
                    ('Status',int(linelist[8]))
                    ])
        return msg

    elif ' NnConfig' == linelist[1] :   
        #Timestamp, NnConfig, MessageID, BootMode, LocationInfo, AutosendLDB, EnableNNRTP, AutosendInterval, IncludeAnchor, IncludeMobile, SortLDB, SolverMinREE, SolverMaxREE, RTPMaxLEE, Persist
        #1444064949.003, NnConfig, 21,   2,        True,         False,       False,       0,                False,         False,         0,       100,          1900,         2000,      True
        msg = dict([('msgType','NnConfig'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('BootMode',int(linelist[3])),    
                    ('LocationInfo',bool(linelist[4])),  
                    ('AutosendLDB',bool(linelist[5])),
                    ('EnableNNRTP',bool(linelist[6])),
                    ('AutosendInterval',int(linelist[7])),
                    ('IncludeAnchor',bool(linelist[8])),
                    ('IncludeMobile',bool(linelist[9])),
                    ('SortLDB',int(linelist[10])),
                    ('SolverMinREE',int(linelist[11])),
                    ('SolverMaxREE',int(linelist[12])),
                    ('RTPMaxLEE',int(linelist[13])),
                    ('Persist',bool(linelist[14]))
                    ])
        return msg

    elif ' NnLocationMap' == linelist[1] :
        return []

    elif ' NnLocationMapEntry' == linelist[1] :   
        #Timestamp, NnLocationMapEntry, NodeID, NodeType, XFixed, YFixed, ZFixed, ZHemisphere, SendELRs, ReportELRs, SendELLs, ReportELLs, BeaconIntervalMs, X, Y, Z
        #1444064949.005, NnLocationMapEntry, 108, 1,      False,   False, False,  2,           False,    False,      1,        True,       0, 1116, 192, 2554
        nodeTypeI = int(linelist[3])
########################################        
#        pdb.set_trace()       
########################################
#        if nodeTypeI > 1 :
#            raise ValueError('Unrecognized NodeType in NnLocationMapEntry')
        nodeTypeList = ['Mobile','Anchor','Origin','+X','-X','+Y','-Y','Z']
        msg = dict([('msgType','NnLocationMapEntry'),
                    ('Timestamp',float(linelist[0])), 
                    ('NodeID',int(linelist[2])),
                    ('NodeType',nodeTypeList[nodeTypeI]),#int(linelist[3])),    
                    ('XFixed',bool(linelist[4])),  
                    ('YFixed',bool(linelist[5])),
                    ('ZFixed',bool(linelist[6])),
                    ('ZHemisphere',int(linelist[7])),
                    ('SendELRs',bool(linelist[8])),
                    ('ReportELRs',bool(linelist[9])),
                    ('SendELLs',int(linelist[10])),
                    ('ReportELLs',bool(linelist[11])),
                    ('BeaconIntervalMs',int(linelist[12])),
                    ('X',float(linelist[13])/1000.0),
                    ('Y',float(linelist[14])/1000.0),
                    ('Z',float(linelist[15])/1000.0)
                    ])
        return msg

    elif ' NnWaypoints' == linelist[1] :
        return []

    elif ' NnWaypointEntry' == linelist[1] :   
        #Timestamp, NnWaypointEntry, ID, X, Y, Z
        #1444064949.007, NnWaypointEntry, 1, 1038, 1286, 1233        
        msg = dict([('msgType','NnWaypointEntry'),
                    ('Timestamp',float(linelist[0])), 
                    ('ID',int(linelist[2])),
                    ('NodeType','Waypoint'),
                    ('X',float(linelist[3])/1000.0),    
                    ('Y',float(linelist[4])/1000.0),  
                    ('Z',float(linelist[5])/1000.0),
                    ])
        return msg

    elif ' RcmRangeInfo' == linelist[1] :
        #Timestamp, RcmRangeInfo, MessageId, ResponderId, RangeStatus, ReqAntennaMode, RespAntennaMode, StopwatchTime, PrecisionRangeMm, CoarseRangeMm, FilteredRangeMm, PrecisionRangeErrEstMm, CoarseRangeErrEstMm, FilteredRangeErrEstMm, FilteredRangeVelocityMmPerSec, FilteredRangeVelocityMmPerSecErrEst, RangeMeasurementType, ReqLEDFlags, RespLEDFlags, Noise, Vpeak, CoarseTOFInBins, EmbeddedTimestamp
        #1444409248.130, RcmRangeInfo, 1552, 114, 0, 1, 1, 23, 11819, 11819, 11819, 55, 55, 58, -1, 173, PCF, 8, 8, 248, 8690, 42473, 1154107
        msg = dict([('msgType','RcmRangeInfo'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('ResponderID',int(linelist[3])),    
                    ('RangeStatus',int(linelist[4])),    
                    ('ReqAntennaMode',int(linelist[5])),    
                    ('RespAntennaMode',int(linelist[6])),    
                    ('StopwatchTime',int(linelist[7])),    
                    ('PrecisionRange',float(linelist[8])/1000.0),    
                    ('CoarseRangeMm',int(linelist[9])),    
                    ('FilteredRangeMm',int(linelist[10])),  
                    ('PrecisionRangeErrEst',float(linelist[11])/1000.0),
                    ('CoarseRangeErrEstMm',int(linelist[12])),
                    ('FilteredRangeErrEstMm',int(linelist[13])),    
                    ('FilteredRangeVelocityMmPerSec',int(linelist[14])),    
                    ('FilteredRangeVelocityMmPerSecErrEst',int(linelist[15])),    
                    ('RangeMeasurementType',linelist[16][1:]),    
                    ('ReqLEDFlags',int(linelist[17])),    
                    ('RespLEDFlags',int(linelist[18])),    
                    ('Noise',int(linelist[19])),    
                    ('Vpeak',int(linelist[20])),
                    ('CoarseTOFInBins',int(linelist[21])),
                    ('EmbeddedTimestamp',long(linelist[21])),
                    ])
        return msg    

    elif ' NnLocationInfo' == linelist[1] :
        #Timestamp, NnLocationInfo, MessageID, NodeID, NodeType, SolverStatus, XFixed, YFixed, ZFixed, ZHemisphere, X, Y, Z, XVariance, YVariance, ZVariance, XYCovariance, XZCovariance, YZCovariance, LocationErrorEstimate, LocationTimestampMs, MessageTimestamp
        msg = dict([('msgType','NnLocationInfo'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('NodeID',int(linelist[3])),    
                    ('NodeType',int(linelist[4])),    
                    ('SolverStatus',int(linelist[5])),    
                    ('XFixed',bool(linelist[6])),    
                    ('YFixed',bool(linelist[7])),    
                    ('ZFixed',bool(linelist[8])),    
                    ('ZHemisphere',int(linelist[9])),    
                    ('X',float(linelist[10])/1000.0),  
                    ('Y',float(linelist[11])/1000.0),
                    ('Z',float(linelist[12])/1000.0),
                    ('XVariance',int(linelist[13])),    
                    ('YVariance',int(linelist[14])),    
                    ('ZVariance',int(linelist[15])),    
                    ('XYCovariance',int(linelist[16])),    
                    ('XZCovariance',int(linelist[17])),    
                    ('YZCovariance',int(linelist[18])),    
                    ('LocationErrorEstimate',int(linelist[19])),    
                    ('LocationTimestampMs',long(linelist[20])),
                    ('MessageTimestamp',long(linelist[21])),
                    ])
        return msg    
        
    elif ' RcmEchoedRangeInfo' == linelist[1] :
        #Timestamp, RcmEchoedRangeInfo, MessageId, RequesterId, ResponderId, PrecisionRangeMm, PrecisionRangeErrEstMm, LEDFlags, EmbeddedTimestamp
        #1443048424.559, RcmEchoedRangeInfo, 23686, 108,        117,         8475,             1345,                   6139,     895373747
        msg = dict([('msgType','RcmEchoedRangeInfo'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('RequesterID',int(linelist[3])),    
                    ('ResponderID',int(linelist[4])),  
                    ('PrecisionRangeMm',int(linelist[5])),
                    ('LEDFlags',int(linelist[6])),
                    ('EmbeddedTimestamp',long(linelist[7])),
                    ])
        return msg

    elif ' NnEchoedLocationInfo' == linelist[1] :
        #Timestamp, NnEchoedLocationInfo, MessageId, NodeID, X, Y, Z, RemoteTimestamp
        #1444064949.218, NnEchoedLocationInfo, 2162, 118, 1091, 992, 1501, 647714
        msg = dict([('msgType','NnEchoedLocationInfo'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('NodeID',int(linelist[3])),    
                    ('X',float(linelist[4])/1000.0),  
                    ('Y',float(linelist[5])/1000.0),
                    ('Z',float(linelist[6])/1000.0),
                    ('RemoteTimestamp',long(linelist[7])),
                    ])
        return msg    
        
    elif ' NnEchoLastLocationExInfo' == linelist[1] :
        #Timestamp, NnEchoLastLocationExInfo, MessageID, NodeID, NodeType, SolverStatus, XFixed, YFixed, ZFixed, ZHemisphere, X, Y, Z, XVariance, YVariance, ZVariance, XYCovariance, XZCovariance, YZCovariance, LocationErrorEstimate, LocationTimestampMs
        #1444065079.052, NnEchoLastLocationExInfo, 117, 118,     0,        2,            False,  False,  False,  2,     8957, 1804, 1501, 3619,   4731,      0,         3498,         0,            0,            1397,                  777586
        msg = dict([('msgType','NnEchoLastLocationExInfo'),
                    ('Timestamp',float(linelist[0])), 
                    ('MessageID',int(linelist[2])),
                    ('NodeID',int(linelist[3])),    
                    ('NodeType',int(linelist[4])),    
                    ('SolverStatus',int(linelist[5])),    
                    ('XFixed',bool(linelist[6])),    
                    ('YFixed',bool(linelist[7])),    
                    ('ZFixed',bool(linelist[8])),    
                    ('ZHemisphere',int(linelist[9])),    
                    ('X',float(linelist[10])/1000.0),  
                    ('Y',float(linelist[11])/1000.0),
                    ('Z',float(linelist[12])/1000.0),
                    ('XVariance',int(linelist[13])),    
                    ('YVariance',int(linelist[14])),    
                    ('ZVariance',int(linelist[15])),    
                    ('XYCovariance',int(linelist[16])),    
                    ('XZCovariance',int(linelist[17])),    
                    ('YZCovariance',int(linelist[18])),    
                    ('LocationErrorEstimate',int(linelist[19])),    
                    ('LocationTimestampMs',long(linelist[20])),
                    ])
        return msg    

    elif ' LogfileMarker' == linelist[1] :
        msg = dict([('msgType','LogfileMarker'),
                    ('Timestamp',float(linelist[0])), 
                    ('markerNum',int(linelist[2]))
                    ])
        return msg
          
    elif ' GameConfig' == linelist[1] :
        #Timestamp, GameConfig, WalkerInitials, CallerInitials, Waypoint1, Waypoint2, Waypoint3
        #1444409248.074, GameConfig, AA, BB, 2, 3, 1
        msg = dict([('msgType','GameConfig'),
                    ('Timestamp',float(linelist[0])), 
                    ('WalkerInitials',linelist[2][1:]),
                    ('CallerInitials',linelist[3][1:]),
                    ('Waypoint1',int(linelist[4])),
                    ('Waypoint2',int(linelist[5])),
                    ('Waypoint3',int(linelist[6])),
                    ])
        return msg
          
        
    elif ' AsConfig' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
        print '\nAsConfig: ', linelist[2:]             
        timestamp = float(linelist[0])
        nodeCount = int(linelist[2])
        
        msg.type = 'AsConfig'
        msg.timestamp = timestamp
        msg.nodeCount = nodeCount
        
    elif ' AsRawNodes' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
        locArray = np.array(map(float,linelist[2:]))
        print 'AsRawNodes: ', ['%.4f' % i for i in map(float,locArray.flatten())]
        print 'pyLocRaw: ', ['%.4f' % i for i in asv.locRaw.transpose().flatten()]                
        #Timestamp, AsRawNodes, Ao_X, Ao_Y, Ao_Z, Ax_X, Ax_Y, Ax_Z, Ay_X, Ay_Y, Ay_Z
        #1443048433.865, AsRawNodes, 0, 0, 2557, 100, 449, 2554, 100, 100, 2549
#                timestamp = float(linelist[0])
#                print timestamp
        locArray = np.reshape(locArray,(asv.nodeCount,3)).T

    elif ' AsFilteredNodes' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
        asFiltArray = np.array(map(float,linelist[2:]))
        print 'AsFilteredNodes: ', ['%.4f' % i for i in asFiltArray]
        pyFiltArray = asv.locFilt.transpose().flatten()
        print 'pyLocFilt: ', ['%.4f' % i for i in pyFiltArray]
        diff = asFiltArray - pyFiltArray
        print 'diff: ', ['%.4f' % i for i in diff]
        #Timestamp, AsFilteredNodes, Ao_X, Ao_Y, Ao_Z, Ax_X, Ax_Y, Ax_Z, Ay_X, Ay_Y, Ay_Z
        #1443048434.929, AsFilteredNodes, 0, 0, 2557, 835, 449, 2554, 194, 601, 2549
        locArray = np.reshape(asFiltArray,(asv.nodeCount,3)).transpose()

    elif ' AsRangeList' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
        asRangeList = np.array(map(float,linelist[2:]))
        print 'AsRangeList: ', asRangeList.tolist()
        #Timestamp, AsRangeList, AoAx_Range, AoAx_RangeErr, AoAy_Range, AoAy_RangeErr, AxAy_Range, AxAy_RangeErr
        #1443048434.929, AsRangeList, 7459, 630, 5211, 1259, 8481, 1290
        print 'pyRangeList:', zip(asv.rangeList.tolist(),asv.reeList.tolist())
    
    elif ' AsWeights' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
#                asWeights = np.array(map(float,linelist[2:]))
        print 'AsWeights: ',['%.4f' % i for i in map(float,linelist[2:])]
        print 'pyWeights: ',['%.4f' % i for i in asv.rWeights]
        #Timestamp, AsWeights, AoAx_Weight, AoAy_Weight, AxAy_Weight
        #1443048434.929, AsWeights, 5.05, 2.53, 2.46

    elif ' AsLocResidual' == linelist[1] :
########################################        
        pdb.set_trace()       
########################################
        print 'AsLocResidual: %5.3f' % float(linelist[2])
        ilocRes += 1
        print 'pyLocResidual: %.4f' % asv.locResidual
        locResidual_saved.extend([float(linelist[2])])
        asv.locResThresh = float(linelist[3])  # this should be put in config
        #Timestamp, AsLocResidual, LocResidual, LocResidualThreshold
        #1443048434.929, AsLocResidual, 7345

    else :
        print 'UNKNOWN LOGFILE ENTRY: ', linelist[1]
########################################        
        pdb.set_trace()       
########################################
        