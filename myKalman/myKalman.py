#!/usr/local/bin/python
from math import sqrt
import numpy as np
#from numpy.linalg import inv
from numpy import dot
#import os
#import itertools
#import matplotlib
#matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#from matplotlib.widgets import Button
#import Tkinter, tkFileDialog
#import time
#import pylab
import pdb
#import sys, getopt, tty, termios
#from processMsg import processMsg
from plot_covariance_ellipse import plot_covariance_ellipse
from numpy.random import randn


class EKF2D :

    X = []
    P = []
    ree = []
    aee = []
    zFixed = []
    anchorLoc = []
    t_prev = []
    locErrEst = []
    dt = []

    def __init__(self, X=X, P=P, aee=aee, zFixed=zFixed) :

        if X :
            self.X = X
        else :
            self.X = np.array([0, 0, 0, 0]).T
            
        if P :
            self.P = P
        else :
            self.P = np.array([[5., 0., 0., 0.],
                               [0., 1., 0., 0.],
                               [0., 0., 5., 0.],
                               [0., 0., 0., 1.]])                          
        self.aee = aee
        self.zFixed = zFixed
        
        
    def predict(self, dt=None, aee=None) :

        if aee == None :
            aee = self.aee
            
        F = np.array([[1., dt, 0., 0.],
                      [0., 1., 0., 0.],
                      [0., 0., 1., dt],
                      [0., 0., 0., 1.]])
        self.X = np.dot(F,self.X)
        
        Q = np.array([[dt**4./3.,  dt**3./2.,         0.,         0.],
                      [dt**3./2.,     dt**2.,         0.,         0.],
                      [0.,                0.,  dt**4./3.,  dt**3./2.],
                      [0.,                0.,  dt**3./2.,     dt**2.]]) * aee**2.
                      
        self.P = dot(dot(F,self.P),F.T) + Q    

    def update(self, dt, r_meas, ree, refLoc) :
        rp = sqrt(  (self.X[0]    - refLoc[0])**2.
                  + (self.X[2]    - refLoc[1])**2.
                  + (self.zFixed  - refLoc[2])**2.)
        #print 'rp ', rp

        # update the linearized measurement matrix
        H = np.array([(self.X[0] - refLoc[0])/rp, 0., (self.X[2] - refLoc[1])/rp, 0.])
        #print 'H ', H

        # Update the Kalman Gain - the normalized ratio of confidence in modeled meas vs. actual meas
        K = dot(self.P,H.T)/(dot(dot(H,self.P),H.T) + ree**2)
        #print 'K ', K

        # Update the State and Covariance estimates
        innovation = r_meas - rp
        self.X = self.X + K*innovation
        self.P = dot((np.eye(4) - np.outer(K.T,H)),self.P)
        self.locErrEst = abs(innovation) + sqrt(np.trace(self.P))

        print 'EKF X: ', self.X
        print 'EKF X: [%.4f, %.4f, %.4f, %.4f]' % (self.X[0],self.X[1],self.X[2],self.X[3])
        print 'EKF P: '
        for irow in range(0,4) :
            print '[%.4f, %.4f, %.4f, %.4f]' % (self.P[irow][0],self.P[irow][1],self.P[irow][2],self.P[irow][3])
        print 'LEE  : ', self.locErrEst

if __name__ == "__main__" :
    
    sigma_range = 0.05
    dt = 0.1

    anchorMap = np.array([[-5, -2, 3], [1, 5, 3]])
    nAnchors,nDims = anchorMap.shape

    nPath = 15
    xPath = np.arange(nPath)
    yPath = (xPath**2)/float(nPath)
    zPath = np.zeros(nPath)
    simPath = np.vstack((xPath,yPath,zPath)).T

    ekf = EKF2D(X=[.1,0,-.1,0],aee=0.1,zFixed=0)

    plt.ion()
    
    fig = plt.figure(figsize=(17,10))    
    ax1 = plt.subplot(1,2,1)    
    ax1.scatter(anchorMap[:, 0], anchorMap[:, 1], marker='s',s=50)
    ax1.axis('equal')
    ax1.grid()
    ax1.set_title("EKF")

    ax2 = plt.subplot(1,2,2)
    ax2.scatter(anchorMap[:, 0], anchorMap[:, 1], marker='s',s=50)
    ax2.axis('equal')
    ax2.grid()
    ax2.set_title("UKF ")

########################################        
#    pdb.set_trace()       
########################################
    for k in range(0,nPath) :
        t = k*dt
    #    x_sim, y_sim = simLoc[k]
        plt.sca(ax1)
        plt.plot(simPath[k,0],simPath[k,1],marker='d',markerfacecolor='r',markersize=5,hold='True')
        plt.draw()

        ekf.predict(dt=dt)
        plot_covariance_ellipse((ekf.X[0], ekf.X[2]), ekf.P[::2,::2], std=6, facecolor='b', alpha=0.08)
    
        iAnchor = k % nAnchors
        anchorLoc = anchorMap[iAnchor]
        xt, yt, zt = simPath[k][0], simPath[k,1], simPath[k,2]
        dx, dy, dz = anchorLoc[0]-xt, anchorLoc[1]-yt, anchorLoc[2]-zt
        range_error = randn()*sigma_range
        r_sim = sqrt(dx**2 + dy**2 + dz**2) + range_error
    
        ekf.update(dt=dt, r_meas=r_sim, ree=sigma_range, refLoc=anchorLoc)
    #     dt,rMeas,ree,anchorLoc
    #
        plot_covariance_ellipse((ekf.X[0], ekf.X[2]), ekf.P[::2,::2], std=6, facecolor='g', alpha=0.4)
        plt.draw()
    #
    #plt.plot(track[:, 0],track[:,1],'o--',color='0.4',lw=2,markersize=8)
    print('final covariance', ekf.P.diagonal())
########################################        
    pdb.set_trace()       
########################################
